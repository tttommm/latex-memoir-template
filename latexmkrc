# latexmkrc
# Copyright 2018 Tom van Rijn
#
# This work may be distributed and/or modified under the
# conditions of the LaTeX Project Public License, either version 1.3
# of this license or (at your option) any later version.
# The latest version of this license is in
# http://www.latex-project.org/lppl.txt
# and version 1.3 or later is part of all distributions of LaTeX
# version 2005/12/01 or later.
#
# This work has the LPPL maintenance status `maintained'.
#
# The Current Maintainer of this work is Tom van Rijn.
#
# This work consists of all files listed in manifest.txt and
# their derived files.

@default_files = ('main.tex');
$silent = 1;
$pdf_mode = 1; # tex -> pdf
$pdflatex = 'pdflatex -synctex=1 -shell-escape';
# $pdflatex = 'xelatex -synctex=1 -shell-escape';
$bib_program = 'biber';
# $bibtex_use = 2; # might not be suitable for everything, used to delete bbl file but might do other thinks
# $makeindex = 'texindy %O -o %D %S';
$makeindex = 'texindy';

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');
add_cus_dep('syg', 'syi', 0, 'run_makeglossaries');

sub run_makeglossaries {
	if ( $silent ) {
		system "makeglossaries -q '$_[0]'";
	}
	else {
		system "makeglossaries '$_[0]'";
	};
}

# not sure about this part
add_cus_dep('idx', 'ind', 0, 'texindy');
sub texindy{
		system("texindy \"$_[0].idx\""); #$
}

@generated_exts = (@generated_exts, 'synctex.gz');

push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
push @generated_exts, 'syg', 'syi', 'slg';
push @generated_exts, 'run', 'lol', 'mw';
push @generated_exts, 'idx', 'ind';
$clean_ext .= ' %R.ist %R.xdy';


# Alternative way to makeglossaries
# add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
# add_cus_dep('acn', 'acr', 0, 'makeglo2gls');
# sub makeglo2gls {
#	my ($base_name, $path) = fileparse( $_[0] );
#	pushd($path);
#	system("makeglossaries $base_name");
#	popd;
# }