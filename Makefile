# Makefile
# % Copyright 2018 Tom van Rijn
#
# This work may be distributed and/or modified under the
# conditions of the LaTeX Project Public License, either version 1.3
# of this license or (at your option) any later version.
# The latest version of this license is in
# http://www.latex-project.org/lppl.txt
# and version 1.3 or later is part of all distributions of LaTeX
# version 2005/12/01 or later.
#
# This work has the LPPL maintenance status `maintained'.
#
# The Current Maintainer of this work is Tom van Rijn.
#
# This work consists of all files listed in manifest.txt and
# their derived files.


# The master file of the document
ROOTFILE = demo
# Folder containing all auxiliary files and folders
AUX_FOLDER = aux
# Compilation Flags
PDFLATEX_FLAGS  = -shell-escape -halt-on-error -synctex=1
PDFLATEX_SILENT_FLAGS  = -shell-escape -halt-on-error -interaction=batchmode -synctex=1
PDFLATEX_SILENT_DRAFT_FLAGS  = -shell-escape -halt-on-error -draftmode -interaction=batchmode -synctex=1
INDEX_FLAGS =
# INDEX_FLAGS = -M basic.xdy $(AUX_FOLDER)/
GLOSSARIES_FLAGS =
BIBER_FLAGS =
# COUNT_FLAGS = -inc -incbib -dir=input -html $(ROOTFILE).tex > wordcount.html
COUNT_FLAGS = -inc -incbib -total -html $(ROOTFILE).tex > wordcount.html

# PDF viewer to use
PDFVIEW = open -a "Skim"

# How to build the PDF
TEX = pdflatex
# TEX = xelatex
BIB = biber
# INDEX = makeindex
INDEX = texindy
GLOSSARY = makeglossaries
COUNT = texcount

# Quiet versions
# TEXQ = texfot --quiet --ignore 'warning.*has been referenced' --ignore 'This is pdfTeX' --ignore '(LaTeX Warning: Citation)' --ignore '(LaTeX Warning: There were undefined references.)' --ignore '(Package biblatex Warning\: Please \(re\)run .* on the file)' --ignore '(LaTeX Warning\: Reference)' --ignore '(Token not allowed in a PDF string)' --ignore '(LaTeX Warning\: Label\(s\) may have changed\. Rerun to get cross\-references right)' --ignore 'Package nag Info' --ignore 'Package nag Warning' --ignore 'Package layouts Warning: Layout scale set to' --ignore 'Output written on' $(TEX)
TEXQ = texfot --quiet --ignore 'This is pdfTeX' --ignore 'Package hyperref Warning' --ignore 'Package layouts Warning: Layout scale set to' --ignore 'Output written on' $(TEX)
# --ignore 'Underfull' --ignore 'Overfull'
# --ignore accepts a Perl regexp
TEX_SILENT = texfot --quiet --ignore 'This is pdfTeX'
BIBQ = $(BIB) --quiet
INDEXQ = $(INDEX) -q
GLOSSARYQ = $(GLOSSARY) -q

# Make the full document in quiet mode (only shows warnings and badboxes)
.PHONY : quiet
quiet :
	@#latexmk
	@#make draft
	@make full

# Make the draft document without lists, table of contents and titlepages
.PHONY : draft
draft :
	@#make clean
	@make singlesilent
	@make wordcount

# Clean and make the full document in quiet mode (only shows warnings and badboxes)
.PHONY : full
full :
	@make cleanfiles
	@make fullnoclean

# Make the full document in quiet mode (only shows warnings and badboxes)
.PHONY : fullnoclean
fullnoclean :
	@make singlesilentdraft
	@make index
	@make singlesilentdraft
	@make glossaries
	@make bib
	@make singlesilentdraft
	@make singlesilent
	@make wordcount

# # Make the full document in quiet mode (only shows warnings and badboxes)
# .PHONY : fullnoclean
# fullnoclean :
# 	@#make singlesilentnopdf
# 	@#make index
# 	@make singlesilentnopdf
# 	@make glossaries
# 	@make bib
# 	@make singlesilentnopdf
# 	@make singlesilentnopdf
# 	@make wordcount

.PHONY : single
single:
	@printf "Running pdflatex\n"
	@$(TEXQ) $(PDFLATEX_FLAGS) $(ROOTFILE).tex

.PHONY : singlesilent
singlesilent:
	@printf "Running pdflatex silently...		"
	@$(TEX_SILENT) $(TEX) $(PDFLATEX_SILENT_FLAGS) $(ROOTFILE).tex
	@printf "DONE\n"

.PHONY : singlesilentdraft
singlesilentdraft:
	@printf "Running pdflatex silently...		"
	@$(TEX_SILENT) $(TEX) $(PDFLATEX_SILENT_DRAFT_FLAGS) $(ROOTFILE).tex
	@printf "DONE\n"

.PHONY : cleansingle
cleansingle:
	@make clean
	@make single

.PHONY : double
double:
	@make single
	@make single

.PHONY : cleandouble
cleandouble:
	@make clean
	@make double

all: $(ROOTFILE).pdf

# View pdf
.PHONY : view
view: $(ROOTFILE).pdf
	@$(PDFVIEW) $(ROOTFILE).pdf

# Make bibliogrphy
.PHONY : bib
bib :
	@printf "Running biber...			"
	@$(BIBQ) $(BIBER_FLAGS) $(ROOTFILE)
	@printf "DONE\n"

# Make index
.PHONY : index
index :
	@printf "Running makeindex...			"
	@$(INDEXQ) $(INDEX_FLAGS)$(ROOTFILE).idx
	@printf "DONE\n"

# Make glossaries
.PHONY : glossaries
glossaries :
	@printf "Running makeglossaries...		"
	@$(GLOSSARYQ) $(GLOSSARIES_FLAGS) $(ROOTFILE)
	@printf "DONE\n"


# Word count
.PHONY : count
count :
	@printf "Running texcount...			"
	@$(COUNT) $(COUNT_FLAGS) $(ROOTFILE).tex
	@printf "DONE\n"

# Word count, save in html file
.PHONY : wordcount
wordcount :
	@printf "Running texcount...			"
	@$(COUNT) $(COUNT_FLAGS)
	@printf "DONE\n"

# Word count, save in html file, view html file
.PHONY : wordcountview
wordcounthtmlview :
	@make wordcount
	@open wordcount.html

# Remove the temporary files
.PHONY : cleanfiles
cleanfiles :
	@printf "Deleting temporary files and folders...	"
	@rm -f *.acn
	@rm -f *.acr
	@rm -f *.alg
	@rm -f *.aux
	@rm -f *.bbl
	@rm -f *.bcf
	@rm -f *.blg
	@rm -f *.fdb_latexmk
	@rm -f *.fls
	@rm -f *.glg
	@rm -f *.glo
	@rm -f *.gls
	@#rm -f *.html
	@rm -f $(ROOTFILE).idx
	@rm -f *.ilg
	@rm -f *.ind
	@rm -f *.ist
	@rm -f *.lof
	@rm -f $(ROOTFILE).log
	@rm -f *.lol
	@rm -f *.lot
	@rm -f $(ROOTFILE).mw
	@rm -f *.out
	@rm -f *.run.xml
	@rm -f *.slg
	@#rm -f *.synctex.gz
	@rm -f *.syg
	@rm -f *.syi
	@rm -f *.tdo
	@rm -f *.toc
	@rm -Rf _minted-main
	@printf "DONE\n"

# Remove the temporary files in auxiliary folder
.PHONY : clean
clean :
	@printf "Deleting temporary files and folders...	"
	@rm -rf $(AUX_FOLDER)
	@rm -Rf _minted-main
	@printf "DONE\n"

# remove everything but the source files
.PHONY : cleanful
cleanful :
	@make clean
	@make cleanfiles
	@rm -f $(ROOTFILE).pdf
	@#rm -f *.pdf
	@rm -Rf figures
