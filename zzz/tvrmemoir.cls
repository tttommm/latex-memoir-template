%% tvrmemoirclass.tex
%% Copyright 2017 Tom van Rijn
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Tom van Rijn.
%
% This work consists of all files listed in manifest.txt and
% their derived files.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tvrmemoir}[2018/02/05 Tom van Rijn memoir class]

% Class options
\DeclareOption{nl}{\@nltrue \@enfalse \@frfalse}
\DeclareOption{en}{\@nlfalse \@entrue \@frfalse }
\DeclareOption{fr}{\@nlfalse \@enfalse \@frtrue}
\newif\if@nl
\newif\if@en
\newif\if@fr
\@nlfalse
\@entrue
\@frfalse

\DeclareOption{draft}{\@finalfalse}
\DeclareOption{final}{\@finaltrue}
\newif\if@final
\newif\if@draft
\@finaltrue
\@finaltrue

\DeclareOption{oneside}{\@twosidefalse}
\DeclareOption{twoside}{\@twosidetrue}
\newif\if@twoside
\newif\if@oneside
\@twosidefalse
\@twosidefalse

\DeclareOption{romanbackmatternumbering}{\@romanbackmatternumberingtrue}
\newif\if@romanbackmatternumbering
\@romanbackmatternumberingfalse

\DeclareOption{gloss}{\@glosstrue}
\newif\if@gloss
\@glossfalse

\DeclareOption{makeidx}{\@makeidxtrue}
\newif\if@makeidx
\@makeidxfalse

\DeclareOption{compacttitles}{\@compacttitlestrue}
\newif\if@compacttitles
\@compacttitlesfalse

\DeclareOption{marginoffset}{\@marginoffsettrue}
\newif\if@marginoffset
\@marginoffsetfalse

\DeclareOption{chapterraggedleft}{\@chapterraggedlefttrue}
\newif\if@chapterraggedleft
\@chapterraggedleftfalse

\DeclareOption{centerfigures}{\@centerfigurestrue}
\DeclareOption{freefigures}{\@centerfiguresfalse}
\newif\if@centerfigures
\newif\if@freefigures
\@centerfigurestrue
\@centerfigurestrue

\DeclareOption{highlightcode}{\@highlightcodetrue}
\newif\if@highlightcode
\@highlightcodefalse

\DeclareOption{tablecaptionontop}{\@tablecaptionontoptrue}
\DeclareOption{tablecaptiononbottom}{\@tablecaptionontopfalse}
\newif\if@tablecaptionontop
\@tablecaptionontoptrue

\DeclareOption{csvsimple}{\@csvsimpletrue}
\newif\if@csvsimple
\@csvsimplefalse


\DeclareOption*{\PackageWarning{tvrmemoir}{Unknown option `\CurrentOption'}}
\ProcessOptions\relax

\if@twoside
	\if@final
		\LoadClass[12pt,twoside,a4paper,extrafontsizes]{memoir}
	\else
		\LoadClass[12pt,twoside,a4paper,extrafontsizes,draft]{memoir}
	\fi

\else
	\if@final
		\LoadClass[12pt,oneside,a4paper,extrafontsizes]{memoir}
	\else
		\LoadClass[12pt,oneside,a4paper,extrafontsizes,draft]{memoir}
	\fi
		\renewcommand\frontmatter{%
		\cleardoublepage
		\@mainmatterfalse
		\pagenumbering{arabic}}
		% \pagestyle{plain}
	\def\pagenumbering#1{\gdef\thepage{\csname @#1\endcsname \c@page}}
\fi

\let\oldmainmatter\mainmatter
\renewcommand\mainmatter{
	\if@twoside
	\if@romanbackmatternumbering
	\newcounter{savepagenumber}
	\setcounter{savepagenumber}{\value{page}}
	\fi
	\fi
	% \pagestyle{plainmarkruled}
	\oldmainmatter}

\let\oldbackmatter\backmatter
\renewcommand\backmatter{\oldbackmatter
	\if@twoside
	\if@romanbackmatternumbering
	\pagenumbering{roman}%
	\setcounter{page}{\value{savepagenumber}}%
	\fi
	\fi
}


\if@final
	\RequirePackage[final]{graphicx}
	\SingleSpacing
\else
	\RequirePackage[draft]{graphicx}
	\DoubleSpacing
\fi


\if@en
	\RequirePackage[english]{babel}
	\RequirePackage[british,showdow]{datetime2}
\fi

\if@fr
	\RequirePackage[french]{babel}
	\RequirePackage[french,showdow]{datetime2}
	\renewcommand{\appendixpagename}{Annexes}
	\renewcommand{\appendixtocname}{Annexes}
\fi

\if@nl
	\RequirePackage[dutch]{babel}
	\RequirePackage[dutch,showdow]{datetime2}
	\renewcommand{\appendixpagename}{Bijlagen}
	\renewcommand{\appendixtocname}{Bijlagen}
\fi

\RequirePackage{morewrites}
% \RequirePackage{palatino} %Change font
\RequirePackage{newpxtext}% newpxmath
\RequirePackage[utf8]{inputenc}% Om niet ascii karakters rechtstreeks te kunnen typen
\RequirePackage[T1]{fontenc}

\RequirePackage{relsize}
\RequirePackage{adjustbox}
\RequirePackage{amssymb}% Voor speciale symbolen zoals de verzameling Z, R...

\RequirePackage{url}% Om url's te verwerken
	\urlstyle{same} % zelfde font als rest document
	%Enable to close url package
	\Urlmuskip=0mu plus 1mu

\RequirePackage{xspace}% Magische spaties na een commando

\RequirePackage{eurosym}                    % om het euro symbool te krijgen
\RequirePackage{textcomp}                   % Voor onder andere graden celsius

\if@csvsimple
	\RequirePackage{tabulary}
	\RequirePackage{tabularx}
	\newcolumntype{K}[1]{>{\raggedleft\arraybackslash}p{#1}}
	\RequirePackage{csvsimple}
	\csvstyle{vgl_ets}{
		tabular=>{\bfseries}p{3.8cm}lp{4.4cm}lp{1.6cm},
		table head=\toprule Etsmiddel & Corrosiviteit & Neutralisatie- en beschikkingsproblemen & Toxiciteit & Totale Kost\\\midrule,
		late after line=\\\hline,
		late after last line=\\\bottomrule,
		head to column names}
	\csvstyle{vgl_ets_tabular}{
		late after line=\\\hline,
		late after last line=\\\bottomrule,
		head to column names}
	\csvstyle{costs}{
		tabular=p{6.4cm}rrK{2.7cm}r,
		separator=semicolon,
		table head=\toprule\bfseries Beschrijving & \bfseries Aantal & \bfseries Eenh.pr. & \bfseries Verzend-/Invoerkosten & \bfseries Prijs\\\midrule,
		table foot=\midrule\bfseries Totaal & & & & \bfseries \euro~1'378.00\\\bottomrule}
	\csvstyle{costs_tabular}{
		separator=semicolon,late after line=\\,
		late after last line=\\\midrule
	}
\fi

%Center all figures

\if@centerfigures
\setfloatadjustment{figure}{\centering}
\fi

\if@makeidx
	\makeindex
\fi

% \RequirePackage[pstricks,squaren,thinqspace,thinspace]{SIunits} % Om elegant eenheden zetten
% \RequirePackage[Gray,squaren,thinqspace,thinspace]{SIunitx} % Om elegant eenheden zetten
\RequirePackage[per-mode=symbol]{siunitx}

%Define fields that can be modified
\gdef\@faculty{}
\gdef\@promotor{}
\gdef\@mentor{}
\gdef\@reason{}
\gdef\@keywords{}
\gdef\@subject{}

\def\faculty#1{\gdef\@faculty{#1}}
\def\promotor#1{\gdef\@promotor{#1}}
\def\mentor#1{\gdef\@mentor{#1}}
\def\reason#1{\gdef\@reason{#1}}
\def\keywords#1{\gdef\@keywords{#1}}
\def\subject#1{\gdef\@subject{#1}}

% needed for biblatex
\RequirePackage[backend=biber,style=ieee,backref=true]{biblatex}%,hyperref, backend=biber,backend=bibtex8, backend=bibtex,defernumbers=true

	% % Work in progress (change author with organization)
	% \DeclareDatamodelEntrytypes{standard}
	% \DeclareDatamodelEntryfields[standard]{type,number}
	% \DeclareBibliographyDriver{standard}{%
	% 	\usebibmacro{bibindex}%
	% 	\usebibmacro{begentry}%
	% 	\usebibmacro{author}%
	% 	\setunit{\labelnamepunct}\newblock
	% 	\usebibmacro{title}%
	% 	\newunit\newblock
	% 	\printfield{number}%
	% 	\setunit{\addspace}\newblock
	% 	\printfield[parens]{type}%
	% 	\newunit\newblock
	% 	\usebibmacro{location+date}%
	% 	\newunit\newblock
	% 	\iftoggle{bbx:url}
	% 	  {\usebibmacro{url+urldate}}
	% 	  {}%
	% 	\newunit\newblock
	% 	\usebibmacro{addendum+pubstate}%
	% 	\setunit{\bibpagerefpunct}\newblock
	% 	\usebibmacro{pageref}%
	% 	\newunit\newblock
	% 	\usebibmacro{related}%
	% 	\usebibmacro{finentry}}

	\def\blx@maxline{77}

	% \DeclareFieldFormat [article] {journaltitle}{\textbf{\textit{#1\isdot}}}
	% \DeclareFieldFormat [online] {title}{\textit{#1}}
	% \if@en
	% \DeclareFieldFormat [online] {url}{[Online] Beschikbaar: \url{#1}}
	% \fi

	\if@nl
	\DeclareFieldFormat [online] {url}{[Online] Beschikbaar: \url{#1}}
	\fi
	\if@fr
	\DeclareFieldFormat [online] {url}{[Online] Disponible: \url{#1}}
	\fi


	\addbibresource{bibliography.bib} % add bibliography!
	\addbibresource{Remote.bib} % add bibliography!

	% % make subcategories cited and non-cited
	% \DeclareBibliographyCategory{cited}
	% \AtEveryCitekey{\addtocategory{cited}{\thefield{entrykey}}}
	% \renewbibmacro*{finentry}{\iflistundef{pageref}{}{\renewcommand{\finentrypunct}{}}\finentry}


	% % customize backref for biblatex
	% \DefineBibliographyStrings{english}{%
	%  backrefpage = {},% originally "cited on page"
	%  backrefpages = {},% originally "cited on pages"
	% }
	% \DefineBibliographyStrings{dutch}{%
	%  backrefpage = {},% originally "cited on page"
	%  backrefpages = {},% originally "cited on pages"
	% }
	% \DefineBibliographyStrings{french}{%
	%  backrefpage = {},% originally "cited on page"
	%  backrefpages = {},% originally "cited on pages"
	% }

	% \newcommand \Dotfill {\leavevmode \cleaders \hb@xt@ .80em{\hss .\hss }\hfill \kern 0.3em}


	% \renewbibmacro*{pageref}{%
	% 	\iflistundef{pageref}
	% 	{}
	% 	{\setunit{\adddot}\printtext{%
	% 		\ifnumgreater{\value{pageref}}{1}
	% 		{\Dotfill\bibstring{backrefpages}}
	% 		{\Dotfill\bibstring{backrefpage}}%
	% 		\printlist[pageref][-\value{listtotal}]{pageref}
	%	}}}

\DefineBibliographyStrings{english}{backrefpage = {cit. on p.}}
\DefineBibliographyStrings{english}{backrefpages = {cit. on p.}}
\DefineBibliographyStrings{dutch}{backrefpage = {cit. op p.}}
\DefineBibliographyStrings{dutch}{backrefpages = {cit. op p.}}
\DefineBibliographyStrings{french}{backrefpage = {cit. sur p.}}
\DefineBibliographyStrings{french}{backrefpages = {cit. sur p.}}

\RequirePackage{rotating} % Provides {sideways}{sidewaysfigure}{sidewaystable} environments

\def\leftsidemargin{25mm}
\def\rightsidemargin{25mm}
\def\topsidemargin{30mm}
\def\bottomsidemargin{30mm}
\def\bindingoffset{15mm}

% Page margins

\setulmarginsandblock{\topsidemargin}{\bottomsidemargin}{*}%{top}{bottom}
	\if@marginoffset
		\setlrmarginsandblock{\dimexpr\leftsidemargin+\bindingoffset\relax}{\rightsidemargin}{*}%{bindingside margin}{other side margin}
	\else
		\setlrmarginsandblock{\leftsidemargin}{\rightsidemargin}{*}%{left margin}{right margin}
	\fi
	% \setheadfoot{29pt}{29pt} %set header and footer height for two line headers and footers
	% \setheadfoot{14.5pt}{14.5pt} %set header and footer height for two line headers
	\setheadfoot{\onelineskip}{2\onelineskip} %set header and footer height for two line headers and footers

\checkandfixthelayout

% pagestyle
\renewcommand{\sectionmark}[1]{\markright{\thesection. \space #1}}
\renewcommand{\chaptermark}[1]{\markboth{\thechapter. \space #1}{}}

\if@twoside
	\makepagestyle{plainmarkruled}
		\makeheadrule{plainmarkruled}{\textwidth}{\normalrulethickness}
		\makeevenhead{plainmarkruled}{\leftmark}{}{}
		\makeoddhead{plainmarkruled}{}{}{\rightmark}
		\makeevenfoot{plainmarkruled}{\thepage}{}{}
		\makeoddfoot{plainmarkruled}{}{}{\thepage}
		% \makepsmarks{plainmarkruled}{\@ruledmarks}
	\nouppercaseheads
\else
	\makepagestyle{plainmarkruled}
		\makeheadrule{plainmarkruled}{\textwidth}{\normalrulethickness}
		\makeevenhead{plainmarkruled}{\leftmark}{}{\rightmark}
		\makeoddhead{plainmarkruled}{\leftmark}{}{\rightmark}
		\makeevenfoot{plainmarkruled}{}{\thepage}{}
		\makeoddfoot{plainmarkruled}{}{\thepage}{}
		% \makepsmarks{plainmarkruled}{\@ruledmarks}
	\nouppercaseheads
\fi


% \pagestyle{plainmarkruled}
% \chapterstyle{companion}
% \chapterstyle{bianchi}%Works like a charm, Chapter is written and chapter number is written as a number.
% \chapterstyle{dash}%Too Small
% \chapterstyle{demo2}%chapter number is written in full in ENGLISH.
% \chapterstyle{veelo}%Nice but has a line on right side of the chapter number which is not nice.
% \chapterstyle{demo3}%

% Voor het organigram
\RequirePackage[svgnames,dvipsnames]{xcolor}
\RequirePackage{tikz, fourier, ifthen}
	\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,fit,positioning,shapes.symbols,chains,calc,shapes,matrix}

	\tikzstyle{line} = [draw, -]
	\tikzstyle{leftarrow} = [draw, <-]
	\tikzstyle{rightarrow} = [draw, ->]

	% Stijlen voor het organigram
	\tikzstyle{blank} = [node distance=1cm, inner sep=0pt, fill,shape=circle,minimum size=2pt]
	\tikzstyle{force} = [rectangle, draw, inner sep=5pt, text width=4cm, text badly centered]%, rounded corners]
	\tikzstyle{comment} = [rectangle, inner sep= 5pt, text width=4cm, node distance=0.25cm, font=\footnotesize\scriptsize\sffamily]
	% Stijl voor de SWOT-analyse (versie in commentaar)
	\tikzstyle{table} = [rectangle, rounded corners, draw, text badly centered, inner sep=0pt]

%Voor piecharts
% \newcommand{\degre}{$^\circ$}
\colorlet{color0}{blue!40}
\colorlet{color1}{orange!60}
\colorlet{color2}{DarkGreen!40}
\colorlet{color3}{yellow!60}
\colorlet{color4}{red!60}
\colorlet{color5}{blue!60!cyan!60}
\colorlet{color6}{cyan!60!yellow!60}
\colorlet{color7}{red!60!cyan!60}
\colorlet{color8}{red!60!blue!60}
\colorlet{color9}{orange!60!cyan!60}



	\tikzset{%
		dc tag/.style={align=center},
		dc legend/.style={align=left,anchor=west},
		dc sector/.style={fill=\Cj,line join=round}
		}

	\pgfkeys{/DiagCirc/.cd,
		% liste of \Name/Value
		value list/.store in=\Value@list,
		% circular : 360 - semi circular 180
		angle max/.store in=\Angle@max,
		angle max=360,
		% radius of the diagram
		radius/.store in=\R@dius,
		radius=4cm,
		% composition of the legend
		% \V value
		% \N name
		% \P percent
		% \A angle
		% \Cj color
		legend/.store in=\L@gend,
		legend=,
		% location of the legend
		legend location/.store in=\Legend@Loc,
		legend location={($(\R@dius,\R@dius)+(1.5,-1)$)},
		% poisition of the node in the sector
		% 0 center, 1 on the edge, 1.++ external
		factor/.store in=\F@ctor,
		factor=.80,
		% composition of the node in the sector
		tags/.store in=\T@gs,
		tags=,
		% correction of round errors in percents
		percent corr/.store in=\C@rrP,
		percent corr=,
		% correction of round errors in angles
		angle corr/.store in=\C@rrA,
		angle corr=,
		% individual shift
		shift sector/.store in=\Shift@j,
		shift sector=,
		% more nodes in the sectors, or new legeng
		sup loop/.store in=\Sup@Loop,
		sup loop=,
		% code of the diagram
		diagram/.code={%
		% Calculation of the sum
		\pgfmathsetmacro\S@m{0}
		\foreach \i/\y in \Value@list {\xdef\S@m{\S@m+\i}}
		\pgfmathsetmacro\S@m{\S@m}
		\pgfmathsetmacro\C@eff{\Angle@max/\S@m}

		% beginning of the first sector
		\xdef\@ngleA{0}

		% main loop
		\foreach \V/\N [count=\j from 0] in \Value@list {%

		% calculation of the current angle
		\pgfmathsetmacro\A{\V*\C@eff}

		% superior limit of the sector
		\pgfmathsetmacro\@ngleB{\@ngleA+\A} ;

		% mean angle
		\pgfmathsetmacro\MedA{(\@ngleA+\@ngleB)/2} ;

		% color
		\pgfmathtruncatemacro\@k{mod(\j,10)}
		\def\Cj{color\@k}

		% individual shift
		\ifthenelse{\equal{\Shift@j}{}}{%
		\edef\Sh@ft{0}}{%
		\pgfmathparse{array({\Shift@j},\j)}
		\edef\Sh@ft{\pgfmathresult}
		}

		% drawing of the sector
		\draw[dc sector,shift={(\MedA:\Sh@ft)}] (0,0)
			-- (\@ngleA:\R@dius) arc (\@ngleA:\@ngleB:\R@dius)
				node[midway] (DC\j) {} -- cycle ;

		% low limit of the next sector
		\xdef\@ngleA{\@ngleB} ;

		% current percent correction
		\pgfmathtruncatemacro\P{round(\V/\S@m*100)}
		\ifthenelse{\equal{\C@rrP}{}}{}{%
			\pgfmathparse{array({\C@rrP},\j)}
			\pgfmathtruncatemacro\P{\P+\pgfmathresult}
		}
		\edef\P{\P\,\%}

		% current angle and corection
		\pgfmathtruncatemacro\A{round(\A)}
		\ifthenelse{\equal{\C@rrA}{}}{}{%
			\pgfmathparse{array({\C@rrA},\j)}
			\pgfmathtruncatemacro\A{\A+\pgfmathresult}
			\edef\A{\A\,$^\circ$}
		}

		% the sector node
		\ifthenelse{\equal{\T@gs}{}}{}{%
		\DiagNode[dc tag]{\F@ctor} {\T@gs} ;
		}

		% the legend
		\ifthenelse{\equal{\L@gend}{}}{}{%
		\begin{scope}[shift=\Legend@Loc]
		\draw[fill=\Cj] (0,-.5*\j)
			rectangle ++(.25,.25) ++(0,-.15)
			node[dc legend] {\strut\L@gend} ;
		\end{scope}
		}

		% some more stuff
		\Sup@Loop ;
		}

		} % end of diagram code
	}


% Node on the \j sector
\newcommand{\DiagNode}[2][]{\node[#1] at ($(0,0)!#2!(DC\j)$)}


\RequirePackage[final]{pdfpages}

\RequirePackage{bclogo}
\RequirePackage{pdflscape} % Use landscape for certain pages pdf in front to turn pages. lscape keeps pages upright whereas pdflscae rotates them for better viewing digitally


\if@highlightcode
	%for code snippets
	\RequirePackage{listings}
	% \usepackage{beramono}
	% \RequirePackage{filecontents}

		% \lstset{
		%   basicstyle=\ttfamily,
		%   breaklines=true,
		%   columns=fullflexible
		% }

		\renewcommand{\lstlistingname}{Code}

		\if@nl
		\newlistof{listoflistings}{lol}{\lstlistingname{}lijst}
		\fi
		\if@en
		\newlistof{listoflistings}{lol}{List of \lstlistingname{}s}
		\fi
		\if@fr
		\newlistof{listoflistings}{lol}{Liste de \lstlistingname{}s}
		\fi


		\newfloat[chapter]{ftlisting}{lol}{\lstlistingname}
		\newlistentry[chapter]{ftlisting}{lol}{0}
		\newfixedcaption{\lstcaption}{ftlisting}

		\newenvironment{code}
		  {\list{}{%
		    \leftmargin=0pt
		    \topsep=6pt
		    \listparindent=\parindent
		    \itemindent=\parindent
		  }\item\relax}
		  {\endlist}


	\RequirePackage[most]{tcolorbox}
	\RequirePackage[newfloat,outputdir=aux,final]{minted}
		% \usemintedstyle{default}
		% \usemintedstyle{trac}
		\usemintedstyle{arduino}
		\definecolor{mintedbackground}{rgb}{0.95,0.95,0.95}
		\newmintedfile[arduinocode]{arduino}{
			%fontfamily=courier,
			fontseries=auto,
			% bgcolor=mintedbackground,
			% fontfamily=tt,
			linenos=true,
			%numberblanklines=true,
			numbersep=12pt,
			numbersep=5pt,
			gobble=0,
			autogobble, %autoindent
			% frame=leftline,
			frame=single,
			% frame=lines,
			% framerule=0.4pt,
			% framesep=2mm,
			funcnamehighlighting=true,
			tabsize=4,
			obeytabs=false,
			mathescape=false
			samepage=false, %with this setting you can force the list to appear on the same page
			showspaces=false,
			showtabs =false,
			texcl=false,
			breaklines=true,
			breakafter=
			}
\fi

\feetatbottom
%\setlength{\footskip}{20pt}

%\RequirePackage{framed} % be able to put frame around figure
\setsecnumdepth{subsubsection}
\settocdepth{subsubsection}

%
% \if@nl
% \renewcommand*{\teenstring}{tien}   % teen
% \renewcommand*\nNameo{\iflowernumtoname n\else N\fi ul}
% % \renewcommand*\nNamec{\iflowernumtoname h\else H\fi onderd}
% % \renewcommand*\nNamem{\iflowernumtoname d\else D\fi duizend}
% % \renewcommand*\nNamemm{\iflowernumtoname m\else M\fi iljoen}
% % \renewcommand*\nNamemmm{\iflowernumtoname m\else M\fi iljard}

% \renewcommand*\nNamei{\iflowernumtoname e\else E\fi en}
% \renewcommand*\nNameii{\iflowernumtoname t\else T\fi wee}
% \renewcommand*\nNameiii{\iflowernumtoname d\else D\fi rie}
% \renewcommand*\nNameiv{\iflowernumtoname v\else V\fi ier}
% \renewcommand*\nNamev{\iflowernumtoname v\else V\fi ijf}
% \renewcommand*\nNamevi{\iflowernumtoname z\else Z\fi es}
% \renewcommand*\nNamevii{\iflowernumtoname z\else Z\fi even}
% \renewcommand*\nNameviii{\iflowernumtoname a\else A\fi cht}
% \renewcommand*\nNameix{\iflowernumtoname n\else N\fi egen}
% \renewcommand*\nNamex{\iflowernumtoname t\else T\fi ien}
% \renewcommand*\nNamexi{\iflowernumtoname e\else E\fi lf}
% \renewcommand*\nNamexii{\iflowernumtoname t\else T\fi waalf}
% \renewcommand*\nNamexiii{\iflowernumtoname d\else D\fi er\teenstring}
% \renewcommand*\nNamexiv{\iflowernumtoname v\else V\fi eer\teenstring}
% \renewcommand*\nNamexv{\iflowernumtoname v\else V\fi ijf\teenstring}
% \renewcommand*\nNamexvi{\iflowernumtoname z\else Z\fi es\teenstring}
% \renewcommand*\nNamexvii{\iflowernumtoname z\else Z\fi even\teenstring}
% \renewcommand*\nNamexviii{\iflowernumtoname a\else A\fi cht\teenstring}
% \renewcommand*\nNamexix{\iflowernumtoname n\else N\fi egen\teenstring}
% \renewcommand*\nNamexx{\iflowernumtoname t\else T\fi wintig}
% % \renewcommand*\nNamexxx{\iflowernumtoname d\else D\fi ertig}
% % \renewcommand*\nNamexl{\iflowernumtoname v\else V\fi eertig}
% % \renewcommand*\nNamel{\iflowernumtoname v\else V\fi ijftig}
% % \renewcommand*\nNamelx{\iflowernumtoname z\else Z\fi estig}
% % \renewcommand*\nNamelxx{\iflowernumtoname z\else Z\fi eventig}
% % \renewcommand*\nNamelxxx{\iflowernumtoname t\else T\fi achtig}
% % \renewcommand*\nNamexc{\iflowernumtoname n\else N\fi egentig}
% \fi
%



\makechapterstyle{mybianchi}{%
	\chapterstyle{default}
	\renewcommand*{\chapnamefont}{\Large}
	\renewcommand*{\chapnumfont}{\HUGE}
	\renewcommand*{\printchaptername}{%
		\chapnamefont\centering\@chapapp}
	\renewcommand*{\printchapternum}{\chapnumfont~\thechapter}
	\renewcommand*{\chaptitlefont}{\Huge}
	\renewcommand*{\printchaptertitle}[1]{%
		\hrule\vskip\onelineskip \centering \chaptitlefont\textbf{##1}\par}
	\renewcommand*{\afterchaptertitle}{\vskip\onelineskip \hrule\vskip
		\afterchapskip}
	\renewcommand*{\printchapternonum}{%
		\vphantom{\chapnumfont \textit{9}}\afterchapternum}}



\makechapterstyle{mybianchiraggedleft}{%
	\chapterstyle{default}
	\renewcommand*{\chapnamefont}{\Large}
	\renewcommand*{\chapnumfont}{\HUGE}
	\renewcommand*{\printchaptername}{%
		\chapnamefont\raggedleft\@chapapp}
	\renewcommand*{\printchapternum}{\chapnumfont\thechapter}
	\renewcommand*{\chaptitlefont}{\Huge}
	\renewcommand*{\printchaptertitle}[1]{%
		\hrule\vskip\onelineskip \raggedleft{\centering} \chaptitlefont\textbf{##1}\par}
	\renewcommand*{\afterchaptertitle}{\vskip\onelineskip \hrule\vskip
		\afterchapskip}
	\renewcommand*{\printchapternonum}{%
		\vphantom{\chapnumfont \textit{9}}\afterchapternum}}



\makechapterstyle{tandhraggedleft}{%
  \setlength{\beforechapskip}{1\onelineskip}%
  \setlength{\afterchapskip}{2\onelineskip \@plus .1\onelineskip
                            \@minus 0.167\onelineskip}%
  \renewcommand*{\printchaptername}{}%
  \renewcommand*{\chapternamenum}{}%
  \renewcommand*{\chapnumfont}{\normalfont\huge\bfseries}%
  \renewcommand*{\printchapternum}{\chapnumfont \thechapter\quad}%
  \renewcommand*{\afterchapternum}{}%
%%%  \renewcommand*{\chaptitlefont}{\chapnumfont\raggedright}}
  \renewcommand*{\chaptitlefont}{\chapnumfont\raggedleft}}




\if@chapterraggedleft
	\if@twoside
		\chapterstyle{mybianchiraggedleft}
		% \chapterstyle{mydemo2}
	\else
		\if@compacttitles
			% \chapterstyle{section}
			\chapterstyle{tandhraggedleft}
		\else
			\chapterstyle{mybianchiraggedleft}
			% \chapterstyle{mydemo2}
		\fi
	\fi
\else
	\if@twoside
		\chapterstyle{mybianchi}
		% \chapterstyle{mydemo2}
	\else
		\if@compacttitles
			% \chapterstyle{section}
			\chapterstyle{tandh}
		\else
			\chapterstyle{mybianchi}
			% \chapterstyle{mydemo2}
		\fi
	\fi
\fi


%----------------------------------------------------------------------------------
% Zelfgeschreven newcommands
	\newcommand{\smem}{\\\footnotesize\emph}
	\newcommand{\fig}{figuur \ref}
	\newcommand{\Fig}[1]{(Figuur \ref{#1})}
	\newcommand{\fresh}{\emph{Freshlocker }}
	\renewcommand{\labelitemii}{$\Box$} %item in item is een vierkant met zwarte zijden i.p.v standaard (*)
	\newcommand{\myfrac}[2]{\(\dfrac{\textrm{#1}}{\textrm{#2}}\)}
	\newcommand{\emhf}[1]{{\pagestyle{empty}#1\clearpage}}
	% \newcommand{\opdr}{\paragraph{Opdracht}}
	% \newcommand{\vraa}{\paragraph{Vraag}}
	% \newcommand{\antw}{\paragraph{Antwoord}}
	% \newcommand{\uitw}{\paragraph{Uitwerking}}
	\newcommand{\vi}{v_{in}}
	\newcommand{\vo}{v_{out}}
	\newcommand{\vcc}{V_{cc}}
	\newcommand{\att}{\LARGE{{\color{red}ATTENTION}}\normalsize}

	\newcommand{\FIg}[1]{(Figuur \ref{#1})}
	\newcommand{\pie}{piëzo-elektrisch element }
	\newcommand{\pies}{piëzo-elektrische elementen }

	\newcommand{\mytoc}{
		\begin{SingleSpacing}
		\cleardoublepage
		\tableofcontents
		\cleardoublepage
		\end{SingleSpacing}
		}

	\newcommand{\doubletoc}{
		\begin{SingleSpacing}
		\cleardoublepage
		\setupshorttoc
		\tableofcontents
		\cleardoublepage
		\setupmaintoc
		\tableofcontents
		\cleardoublepage
		\end{SingleSpacing}
		}

	% \newcommand{\mylof}{\begin{SingleSpacing}
	% 	\cleardoublepage\listoffigures
	% \end{SingleSpacing}}

	\addtodef{\listoffigures}{\begin{SingleSpacing}\cleardoublepage}{\end{SingleSpacing}}

	% \newcommand{\mylot}{\begin{SingleSpacing}
	% 	\cleardoublepage\listoftables
	% \end{SingleSpacing}}

	\addtodef{\listoftables}{\begin{SingleSpacing}\cleardoublepage}{\end{SingleSpacing}}

	\addtodef{\listoflistings}{\begin{SingleSpacing}\cleardoublepage}{\end{SingleSpacing}}

	\addtodef{\printindex}{\begin{SingleSpacing}\cleardoublepage\phantomsection}{\end{SingleSpacing}}

	\addtodef{\frontmatter}{}{\pagestyle{plain}}
	\addtodef{\mainmatter}{}{\pagestyle{plainmarkruled}}
	\addtodef{\backmatter}{}{\SingleSpacing\pagestyle{plain}}


% \if@nl
% 	\newcommand{\mybib}{\nocite{*}
% 		\begin{SingleSpacing}
% 			\printbibheading[title={Referenties},heading=bibintoc]
% 			\printbibliography[title={Geciteerd},category=cited,heading=subbibliography]
% 			\thispagestyle{plain}
% 			\newpage
% 			\phantomsection
% 			\DeclareFieldFormat{labelnumberwidth}{}%Remove numbering from non cited references
% 			% \setlength{\biblabelsep}{0pt}
% 			\printbibliography[title={Niet geciteerd},notcategory=cited,heading=subbibliography]%further reading
% 			\thispagestyle{plain}
% 		\end{SingleSpacing}
% 		}
% \fi
% \if@fr
% 	\newcommand{\mybib}{\nocite{*}
% 		\begin{SingleSpacing}
% 			\printbibheading[title={Bibliographie},heading=bibintoc]
% 			\printbibliography[title={Citées},category=cited,heading=subbibliography]
% 			\thispagestyle{plain}
% 			\newpage
% 			\DeclareFieldFormat{labelnumberwidth}{}%Remove numbering from non cited references
% 			% \setlength{\biblabelsep}{0pt}
% 			\printbibliography[title={Non-citées},notcategory=cited,heading=subbibliography]%further reading
% 			\thispagestyle{plain}
% 		\end{SingleSpacing}
% 		}
% \fi
% \if@en
% 	\newcommand{\mybib}{\nocite{*}
% 		\begin{SingleSpacing}
% 			\printbibheading[title={References},heading=bibintoc]
% 			\printbibliography[title={Cited References},category=cited,heading=subbibliography]
% 			\thispagestyle{plain}
% 			\newpage
% 			\DeclareFieldFormat{labelnumberwidth}{}%Remove numbering from non cited references
% 			% \setlength{\biblabelsep}{0pt}
% 			\printbibliography[title={Non-Cited References},notcategory=cited,heading=subbibliography]%further reading
% 			\thispagestyle{plain}
% 		\end{SingleSpacing}
% 		}
% \fi


	\newcommand{\kop}{koperchloride~}
	\newcommand{\cu}{CuCl\subsc{2}}
	\newcommand{\arduino}{\emph{Arduino}~}

	% Super en subscript
	\newcommand{\supsc}[1]{\ensuremath{^{\text{#1}}}}   % Superscript in tekst
	\newcommand{\subsc}[1]{\ensuremath{_{\text{#1}}}}   % Subscript in tekst

	% Niew commando om vreemde taal weer te geven (hint: dit commando kan gebruikt
	%   worden om latijnse namen, die ook cursief moeten staan, weer te geven.
	\newcommand{\engels}[1]{\textit{#1}\xspace}
	\newcommand{\engelsx}[1]{\index{#1}\textit{#1}\xspace}

	% Niew commando om iets te benadrukken en tegelijkertijd in de index te steken.
	\newcommand{\begrip}[1]{\index{#1}\textbf{#1}\xspace}

% De splitsingsuitzonderingen
\hyphenation{back-slash split-sings-uit-zon-de-ring} % zelf aan te vullen voor custom splitsingen

\graphicspath{{images/}} % De plaats waar latex zijn figuren gaat halen.

%\newlength\sibdist
%\setlength\sibdist{1cm}
%\newsavebox\sibsavebox


% %%%% Short and long ToC

  \setlength{\cftpartnumwidth}{1.5cm}%
\newcommand*{\setupshorttoc}{%
  % \renewcommand*{\contentsname}{Short contents}
  \if@nl
    \renewcommand*{\contentsname}{Korte Inhoudsopgave}
  \fi
  \if@fr
    \renewcommand*{\contentsname}{Table des Matières Courte}
  \fi
  \if@en
    \renewcommand*{\contentsname}{Short contents}
  \fi
  % \setlength{\cftbeforechapterskip}{1ex}
  \let\oldchangetocdepth\changetocdepth
  \let\oldprecistoctext\precistoctext
  \renewcommand{\precistoctext}[1]{}
  \let\oldcftchapterfillnum\cftchapterfillnum
  \renewcommand*{\changetocdepth}[1]{}
  \setcounter{tocdepth}{0}% chapters
  \renewcommand*{\cftchapterfont}{\hfill\sffamily}
  \renewcommand*{\cftchapterpagefont}{\normalfont}
  \renewcommand*{\cftchapterleader}{ \textperiodcentered\space}
  \renewcommand*{\cftchapterafterpnum}{\cftparfillskip}
%%  \setpnumwidth{0em}
%%  \setpnumwidth{1.5em}
  \renewcommand*{\cftchapterfillnum}[1]{%
    {\cftchapterleader}\nobreak
    \hbox to 1.5em{\cftchapterpagefont ##1\hfil}\cftchapterafterpnum\par}
  \setrmarg{0.15\textwidth}
  \setlength{\unitlength}{\@tocrmarg}
  \addtolength{\unitlength}{1.5em}
  \let\oldcftpartformatpnum\cftpartformatpnum
  \renewcommand*{\cftpartformatpnum}[1]{%
    \hbox to\unitlength{{\cftpartpagefont ##1}}}
  \let\oldcftbookformatpnum\cftbookformatpnum
  \renewcommand*{\cftbookformatpnum}[1]{%
    \hbox to\unitlength{{\cftbookpagefont ##1}}}}

\newcommand*{\setupparasubsecs}{%
  \let\oldnumberline\numberline
  \renewcommand*{\cftsubsectionfont}{\itshape}
  \renewcommand*{\cftsubsectionpagefont}{\itshape}
  \renewcommand{\l@subsection}[2]{
    \ifnum\c@tocdepth > 1\relax
      \def\numberline####1{\textit{####1}~}%
      \leftskip=\cftsubsectionindent
      \rightskip=\@tocrmarg
%%      \advance\rightskip 0pt plus \hsize % uncomment this for raggedright
%%      \advance\rightskip 0pt plus 2em    % uncomment this for semi-ragged
      \parfillskip=\fill
      \ifhmode ,\ \else\noindent\fi
      \ignorespaces
      {\cftsubsectionfont ##1}~{\cftsubsectionpagefont##2}%
       \let\numberline\oldnumberline\ignorespaces
    \fi}}

\AtEndDocument{\addtocontents{toc}{\par}}%%% OK

\newcommand*{\setupmaintoc}{%
  % \renewcommand{\contentsname}{Contents}
  \if@nl
    \renewcommand*{\contentsname}{Inhoudsopgave}
  \fi
  \if@fr
    \renewcommand*{\contentsname}{Table des Matières}
  \fi
  \if@en
    \renewcommand*{\contentsname}{Contents}
  \fi
  \let\changetocdepth\oldchangetocdepth
  \let\precistoctext\oldprecistoctext
  \let\cftchapterfillnum\oldcftchapterfillnum
  \addtodef{\cftchapterbreak}{\par}{}
  \renewcommand*{\cftchapterfont}{\normalfont\sffamily}
  \renewcommand*{\cftchapterleader}{\sffamily\cftdotfill{\cftchapterdotsep}}
  \renewcommand*{\cftchapterafterpnum}{}
  \renewcommand{\cftchapterbreak}{\par\addpenalty{-\@highpenalty}}
  \setpnumwidth{2.55em}
  \setrmarg{3.55em}
  \setcounter{tocdepth}{2}
  \let\cftpartformatpnum\oldcftpartformatpnum
  \addtodef{\cftpartbreak}{\par}{}
  \let\cftbookformatpnum\oldcftbookformatpnum
  \addtodef{\cftbookbreak}{\par}{}
}

% \newcommand*{\setupshorttoc}{%
%   \renewcommand*{\contentsname}{Short contents}
%   \let\oldchangetocdepth\changetocdepth
%   \let\oldprecistoctext\precistoctext
%   \renewcommand{\precistoctext}[1]{}
%   \let\oldcftchapterfillnum\cftchapterfillnum
%   \renewcommand*{\changetocdepth}[1]{}
%   \setcounter{tocdepth}{0}% chapters
%   \renewcommand*{\cftchapterfont}{\hfill\sffamily}
%   \renewcommand*{\cftchapterpagefont}{\normalfont}
%   \renewcommand*{\cftchapterleader}{ \textperiodcentered\space}
%   \renewcommand*{\cftchapterafterpnum}{\cftparfillskip}
% %%  \setpnumwidth{0em}
% %%  \setpnumwidth{1.5em}
%   \renewcommand*{\cftchapterfillnum}[1]{%
%     {\cftchapterleader}\nobreak
%     \hbox to 1.5em{\cftchapterpagefont ##1\hfil}\cftchapterafterpnum\par}
%   \setrmarg{0.3\textwidth}
%   \setlength{\unitlength}{\@tocrmarg}
%   \addtolength{\unitlength}{1.5em}
%   \let\oldcftpartformatpnum\cftpartformatpnum
%   \renewcommand*{\cftpartformatpnum}[1]{%
%     \hbox to\unitlength{{\cftpartpagefont ##1}}}
%   \let\oldcftbookformatpnum\cftbookformatpnum
%   \renewcommand*{\cftbookformatpnum}[1]{%
%     \hbox to\unitlength{{\cftbookpagefont ##1}}}}

% \newcommand*{\setupparasubsecs}{%
%   \let\oldnumberline\numberline
%   \renewcommand*{\cftsubsectionfont}{\itshape}
%   \renewcommand*{\cftsubsectionpagefont}{\itshape}
%   \renewcommand{\l@subsection}[2]{
%     \ifnum\c@tocdepth > 1\relax
%       \def\numberline####1{\textit{####1}~}%
%       \leftskip=\cftsubsectionindent
%       \rightskip=\@tocrmarg
% %%      \advance\rightskip 0pt plus \hsize % uncomment this for raggedright
% %%      \advance\rightskip 0pt plus 2em    % uncomment this for semi-ragged
%       \parfillskip=\fill
%       \ifhmode ,\ \else\noindent\fi
%       \ignorespaces
%       {\cftsubsectionfont ##1}~{\cftsubsectionpagefont##2}%
%        \let\numberline\oldnumberline\ignorespaces
%     \fi}}

% \AtEndDocument{\addtocontents{toc}{\par}}%%% OK

% \newcommand*{\setupmaintoc}{
%   \renewcommand{\contentsname}{Contents}
%   \let\changetocdepth\oldchangetocdepth
%   \let\precistoctext\oldprecistoctext
%   \let\cftchapterfillnum\oldcftchapterfillnum
%   \addtodef{\cftchapterbreak}{\par}{}
%   \renewcommand*{\cftchapterfont}{\normalfont\sffamily}
%   \renewcommand*{\cftchapterleader}{\sffamily\cftdotfill{\cftchapterdotsep}}
%   \renewcommand*{\cftchapterafterpnum}{}
%   \renewcommand{\cftchapterbreak}{\par\addpenalty{-\@highpenalty}}
%   \setpnumwidth{2.55em}
%   \setrmarg{3.55em}
%   \setcounter{tocdepth}{2}
%   \let\cftpartformatpnum\oldcftpartformatpnum
%   \addtodef{\cftpartbreak}{\par}{}
%   \let\cftbookformatpnum\oldcftbookformatpnum
%   \addtodef{\cftbookbreak}{\par}{}
% }

% \renewcommand{\familydefault}{\sfdefault}

\RequirePackage[colorinlistoftodos,prependcaption,obeyFinal]{todonotes}

\RequirePackage{soul}
\if@todonotes@disabled
\newcommand{\hlfix}[2]{#1 }
\else
\newcommand{\hlfix}[2]{\texthl{#1 }\todo{#2}}
\fi

\renewcommand{\listoftodos}[1][\@todonotes@todolistname]
    {\@ifundefined{chapter}{\section*{#1}}{\chapter*{#1}}
	\vspace*{-32pt}%Use 2 following lines if you want consistent soacing after chapter hrule.
	% \hrule
	% \vspace*{0.3cm}
	\todo[inline, nolist]{Default, use \emph{todo} of \emph{hlfix} to highlight the text to fix.}
	\unsure[inline, nolist]{Unsure, use \emph{unsure}}
	\change[inline, nolist]{Change, use \emph{change}}
	\info[inline, nolist]{Info, use \emph{info}}
	\improvement[inline, nolist]{Improvement, use \emph{improvement}}
	\vspace*{-8pt}
	\hspace*{-1.1em}Missing figure, use \emph{missingfig}
	% \missinfig{Missing figure, use \emph{missinfig}}
	% \begin{itemize}
	% 	\item \emph{hlfix} higlights text to be fixed
	% 	\item \emph{missingfig} is used to display missing figure notes with a white background
	% \end{itemize}
	\vspace*{0.2cm}
	\hrule
	\vspace*{1cm}
    \@starttoc{tdo}}

\RequirePackage{xargs}
\newcommandx{\unsure}[2][1=]{\todo[linecolor=red,backgroundcolor=red!25,bordercolor=red,#1]{#2}}
\newcommandx{\change}[2][1=]{\todo[linecolor=blue,backgroundcolor=blue!25,bordercolor=blue,#1]{#2}}
\newcommandx{\info}[2][1=]{\todo[linecolor=OliveGreen,backgroundcolor=OliveGreen!25,bordercolor=OliveGreen,#1]{#2}}
\newcommandx{\improvement}[2][1=]{\todo[linecolor=Plum,backgroundcolor=Plum!25,bordercolor=Plum,#1]{#2}}
\newcommandx{\thiswillnotshow}[2][1=]{\todo[disable,#1]{#2}}
\newcommandx{\missingfig}[2][1=]{\missingfigure[figcolor=white]{#2}}

\if@final
	\newcommand{\mylistoftodos}{}
\else
	\newcommand{\mylistoftodos}{\cleardoublepage
\listoftodos[Notes]
\cleardoublepage}
\fi

\RequirePackage{float}
\if@tablecaptionontop
	\floatstyle{plaintop}
	\restylefloat{table}
\fi
% \restylefloat{figure}
% \restylefloat{arduinocode}
\RequirePackage{microtype}
\RequirePackage{csquotes}
\RequirePackage{vubtitlepage}

\RequirePackage[plainpages=false,final]{hyperref}
% \RequirePackage[all]{hypcap} %Better linking to top of figure when clicking on list element

%Create new command instead of adpating href
\if@twoside
	\let\oldhref\href
	\renewcommand\href[2]{%
	\oldhref{#1}{#2}\footnote{\url{#1}}%
	}
\fi



\if@gloss
	\RequirePackage{calc}
	\newlength\mypagelistwidth
	\newlength\mydesclistwidth
	\newlength\mysymbollistwidth
	\newlength\mynamelistwidth
	\newlength\availablewidth

	\RequirePackage[acronym,toc,automake]{glossaries-extra}% Om een woordenlijst te maken % 	sanitize,acronym,index,symbols,numbers,nomain
	\if@en
		\newglossary[slg]{symbols}{syi}{syg}{List of Symbols} % create add. symbolslist
		\settowidth{\mypagelistwidth}{Page List}
		\settowidth{\mysymbollistwidth}{Symbols}
		\settowidth{\mynamelistwidth}{Notation}
	\fi

	\if@fr
		\newglossary[slg]{symbols}{syi}{syg}{Liste de symboles} % create add. symbolslist
		\settowidth{\mypagelistwidth}{Pages}
		\settowidth{\mysymbollistwidth}{Symbole}
		\settowidth{\mynamelistwidth}{Terme}
	\fi

	\if@nl
		\newglossary[slg]{symbols}{syi}{syg}{Lijst van symbolen} % create add. symbolslist
		\settowidth{\mypagelistwidth}{Pagina\'s}
		\settowidth{\mysymbollistwidth}{Symbool}
		\settowidth{\mynamelistwidth}{Benaming}
	\fi

	\setlength{\mydesclistwidth}{\linewidth-\mysymbollistwidth-\mynamelistwidth-\mypagelistwidth-\tabcolsep-\tabcolsep-\tabcolsep}

	\loadglsentries{glossaries}

	\makeglossaries

	\newglossarystyle{myaltlong4colheader}{%
		\setglossarystyle{long4colheader}%
		\renewenvironment{theglossary}%
			{\begin{longtable}{p{\mynamelistwidth}p{\mydesclistwidth}p{\mysymbollistwidth}p{\mypagelistwidth}}}%
			{\end{longtable}}%
		}
\fi

% renewcommand subglossentry aswell!!
  % \renewcommand{\glossentry}[2]{%
  %   \glsentryitem{##1}\glstarget{##1}{\glossentryname{##1}} &
  %   \glossentrydesc{##1} &
  %   \glossentrysymbol{##1} &
  %   ##2\tabularnewline
  % }%
  % \renewcommand{\subglossentry}[3]{%
  %    &
  %    \glssubentryitem{##2}%
  %    \glstarget{##2}{\strut}\glossentrydesc{##2} &
  %    \glossentrysymbol{##2} & ##3\tabularnewline
  % }%
  % \ifglsnogroupskip
  %   \renewcommand*{\glsgroupskip}{}%
  % \else
  %   \renewcommand*{\glsgroupskip}{ & & & \tabularnewline}%
  % \fi