%Methode

\section{Methode}
De meting van de dichtheid wordt uitgevoerd aan de hand van de meting van de ultrasone golf, gereflecteerd door de tip van \emph{waveguide 1}. Hiervoor worden \pies gebruikt. De \emph{trailing waves} kunnen overlappen met de golf die ons interesseert. Hierdoor is men genoodzaakt experimenteel te bepalen welk deel van het gemeten signaal in het tijdsdomein de nuttige informatie bevat. Dit wordt gedaan door een rechthoekig venster te gebruiken.

\par
Uit het verschil in amplitude van de verzonden en ontvangen golf kan men de reflectiecoëfficiënt berekenen. Deze wordt dan gebruikt om de akoestische impedantie van de vloeistof te bepalen. Tenslotte berekent men de dichtheid van de vloeistof aan de hand van de akoestische impedantie en de snelheid van de golf door de vloeistof.

\par
De akoestische impedantie is een maat voor de weerstand die een materiaal biedt voor een geluidsgolf. De eenheid is \([\si{\pascal\second\per\cubic\metre}]\).
\par
De verzonden puls door de \emph{interface matching layer}-vloeistof \(U_{tr}\) wordt gebruikt voor het meten van de snelheid \(c_3\) van de ultrasone golf in de te meten vloeistof. Dit gebeurt tijdens de calibratie-fase (zie \ref{sec:calibratie}).

\par
Van het ontvangen signaal \(U_r\) wordt uitsluitend de puls gereflecteerd door de tip van de \emph{waveguide} geselecteerd in het tijdsdomein en wordt vervolgens gebruikt om de dichtheid te bepalen van de vloeistof.

\par
Om preciezer te kunnen meten wordt de tip van \emph{waveguide 1} bedekt met een \emph{matching layer} met een dikte van \(\frac{1}{4}\)\supsc{de} golflengte.


\subsection{Het Signaal}
Voor de excitatie van de ultrasone golf werd een 3,3\si{\mega\hertz} sinusoïdaal signaal van drie perioden lang met een Gaussische envelop gebruikt. Deze envelop is nodig zodat het verstuurde en het gereflecteerde signaal van elkaar zouden kunnen worden gescheiden in het tijdsdomein. Men verkrijgt hierdoor een puls.

\subsection{Materiaalkeuze en -vereisten}
De vereisten voor de \emph{matching layer} zijn:
\begin{itemize}
	\item De akoestische impedantie van de \emph{matching layer} moet liggen tussen de impedantie van de \emph{waveguide transducer} en die van de vloeistof.
	\item De laag moet bestand zijn tegen hoge temperatuur en druk en stabiele akoestische eigenschappen vertonen over een bepaald temperatuurbereik.
\end{itemize}
Dit is een moeilijke opgave omdat er maar weinig materialen zijn die aan deze voorwaarden voldoen. Mogelijke materialen voor de \emph{matching layer} zijn:
\begin{itemize}
	\item Duralco 4703
	\item Polybenzimidazole (PBI)
	\item Aluminium poeder
	\item Glas poeder
\end{itemize}

De \emph{waveguides} moeten een redelijke thermische geleidbaarheid hebben. Er kan zowel staal als titanium gebruikt worden.

\subsection{Calibratie \label{sec:calibratie}}
Vooraleer men een meting kan uitvoeren moet de meetopstelling gecalibreerd worden. Men moet hiervoor eenmalig zo precies mogelijk de afstand tussen de tippen van de \emph{waveguides} bepalen. Als men de vertraging van de puls meet in transmissiemodus bij gekende temperatuur dan vind men deze afstand met de volgende formule:
\begin{equation}\label{lk}
{l_k=c_w(T) \cdotp \tau_w(T)}
\end{equation}

%{\small
\begin{itemize}
	\item \(l_k\): Afstand tussen de tippen van de \emph{waveguides}.
	\item \(c_w(T)\): Gekende snelheid van de ultrasone golf in gedestilleerd water bij temperatuur \(T\).
	\item \(\tau_w(T)\): Vertraging in de tijd van de ultrasone puls in gedestilleerd water.
\end{itemize}%}

Er kan nu overgegaan worden naar de calibratie. Hiervoor moeten volgende stappen doorlopen worden:
\begin{enumerate}
	\item De gereflecteerde signalen \(U_r(T)\), \(U_{rw}(T)\) en de \emph{transmitted} signalen \(U_{tr}(T)\), \(U_{trw}(T)\) - verstuurd door gedestilleerd water en de te meten vloeistof - worden opgenomen.
	\item De tijdsvertraging in de te meten vloeistof wordt gemeten in transmissie-modus en de snelheid van de ultrasone longitudinale golf wordt bekomen uit:
		\begin{equation}\label{c3}
		{c_3(T)=\frac{l_k}{\tau_3(T)}}
		\end{equation}
		%{\small
		\begin{itemize}
			\item \(c_3(T)\): Snelheid van de ultrasone golf door het vloeibaar medium.
			\item \(\tau_3(T)\): Vertraging in de tijd van de ultrasone puls in de vloeistof.
		\end{itemize}%}
	\item De ratio's van de spectra van de gereflecteerde signalen \(U_r(T)\) en \(U_{rw}(T)\) - genormaliseerd ten opzichte van gedestilleerd water - worden berekend waarbij het signaal gereflecteerd door gedestilleerd water \(U_{rw}(T)\) als referentie dient.
		\begin{equation}\label{x}
		{x=\left[\frac{FFT(U_r(T))}{FFT(U_{rw}(T))}\right]_{|f=f_0}}
		\end{equation}
	\item De akoestische impedantie \(Z_3\) wordt benaderd met volgende polynomiaal:
		\begin{equation}\label{z3x}
		{Z_3(x)=ax^2+bx+c}
		\end{equation}
		%{\small
		\begin{itemize}
			\item \(Z_3\): Akoestische impedantie van de vloeistof.
			\item \(a\), \(b\) en \(c\) zijn de coëfficiënten afhankelijk van het materiaal op de \emph{waveguide} en de \emph{matching layer}.
		\end{itemize}%}
	\item De calibratiefunctie wordt gevonden:
		\begin{equation}\label{Urrw}
		{\frac{U_r}{U_{rw}}=F(Z_3)}
		\end{equation}
		%{\small
		\begin{itemize}
			\item \(U_r\): Amplitude van het ultrasoon signaal gereflecteerd van de overgang van vast naar vloeibaar/
			\item \(U_{rw}\): Amplitude van signaal gereflecteerd op gedestilleerd water.
			\item \(F\): Calibratiefunctie.
		\end{itemize}%}
		waarbij de akoestische impedantie gegeven wordt door:
		\begin{equation}\label{z3tx}
		{{Z_3(T)=\rho_3(x) \cdotp c_3(T)}}
		\end{equation}
		%{\small
		\begin{itemize}
			\item \(\rho_3(x)\): Dichtheid van het vloeibaar medium.
		\end{itemize}%}
\end{enumerate}
% Als men de vertraging van de puls meet in transmissiemodus bij gekende temperatuur dan kan men de afstand tussen de tippen van de \emph{waveguides} vinden met volgende formule:
% Men kan nu de snelheid vinden in de vloeistof door het resultaat van voorgaande vergelijking in de volgende formule in te vullen:


% \cite{uitdemuur}

% %%%%%%%%%%
% \subsection{Organigram}
% In \fig{tik:organigram} is het organigram van het bedrijf
% weergegeven. Deze functieverdeling is gebaseerd op de beste
% eigenschappen van elk van ons.
% \begin{figure}[H]
% \begin{center}
% \centering \input{Organigram/organigrambare.tex}
% \caption{Vlajo SBP-Organigram}
% \label{tik:organigram}
% \end{center}
% \end{figure}