byte input = 0;
unsigned long momentHoog;
unsigned long momentLaag;
unsigned long deltaLaag;
unsigned long deltaHoog;
int Hgepasseerd;

int aantal = 0;
unsigned long vorigeMilis = 0;

void setup()
{
  for (int i = 4 ; i <= 11; i++)
  {
    pinMode(i, INPUT);
  }
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
  delay(500);
  digitalWrite(13, HIGH);
  momentHoog = millis();
  momentLaag = momentHoog;
  deltaLaag = momentHoog;
  deltaHoog = momentHoog;
  Hgepasseerd = 2; //na startup moeten beide if's kunnen
}

void loop()
{
  for (int n = 0; n < 8; n++)
  {
    bitWrite(input, n, digitalRead(n + 4));
  }
  float waarde = ((float)input) * 5 / 255;
  if ((waarde > 2.8) & (Hgepasseerd != 1))
  {
    aantal++;
    momentHoog = millis();
    Hgepasseerd = 1;
    deltaLaag = momentHoog - momentLaag;
  }
  if ((waarde < 2.2) & (Hgepasseerd != 0))
  {
    momentLaag = millis();
    Hgepasseerd = 0;
    deltaHoog = momentLaag - momentHoog;
  }

  //Versturen
  unsigned long tijd = millis();
  if (vorigeMilis + 1000 <= tijd)
  {
    if (deltaHoog > deltaLaag)
    {
      Serial.print("H:");
    }
    else
    {
      Serial.print("L:");
    }
    int RPM = aantal * 60;
    vorigeMilis = tijd;
    aantal = 0;
    Serial.println(RPM);
  }
}
