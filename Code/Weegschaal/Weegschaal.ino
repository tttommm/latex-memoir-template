#define maxGewicht 1000

const int analogInPin = A0;

void setup()
{
  Serial.begin(9600);
    
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
  delay(500);
  digitalWrite(13, HIGH);
}

void loop()
{
  int ingelezenWaarde = analogRead(analogInPin);
  int gewicht = map(ingelezenWaarde,114,470,0,maxGewicht);
  Serial.print(ingelezenWaarde);
  Serial.print("      ");
  Serial.print(gewicht);
  Serial.println("g");
  delay(500);
}
