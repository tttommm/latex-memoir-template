---
# README For LaTeX Memoir Template
---


### About

|            |                              |
| ----------:|:-----------------------------|
| Title      | LaTeX Memoir Template        |
| Author     | Tom van Rijn                 |
| Date       | 2018-11-24                   |
| Copyright  | Copyright © 2018 Tom van Rijn|
| Version    | 3.0                          |


### LaTeX Project Public License

This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or (at your option) any later version.
The latest version of this license is in [http://www.latex-project.org/lppl.txt](http://www.latex-project.org/lppl.txt) and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Tom van Rijn.

This work consists of all files listed in manifest.txt and their derived files.


### Contents

<!-- MarkdownTOC -->

- [What is this repository for?](#what-is-this-repository-for)
- [How do I get set up?](#how-do-i-get-set-up)
- [Compile options](#compile-options)
	- [Using the makefile in Terminal](#using-the-makefile-in-terminal)
	- [Writing the commands directly in Terminal](#writing-the-commands-directly-in-terminal)
	- [Other options from the makefile](#other-options-from-the-makefile)
- [Document class options](#document-class-options)
- [Document definitions](#document-definitions)
- [Defaults](#defaults)
- [Required Files](#required-files)
- [Optional Files](#optional-files)
- [To-Do Notes](#to-do-notes)
- [Disclaimer](#disclaimer)
- [Remarks](#remarks)
- [To Do](#to-do)
- [Known Issues](#known-issues)

<!-- /MarkdownTOC -->


<a id="what-is-this-repository-for"></a>
### What is this repository for?

* Latex Memoir Custom Class Template
* Class to make a thesis including the title page (default is for the [Vrije Universiteit Brussel](http://www.vub.ac.be))
* Setup your compiler to be able to compile the document
* How-to use the documents
* Class options
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


<a id="how-do-i-get-set-up"></a>
### How do I get set up?
Run `make` in Terminal to remove all auxiliary files and folders, compile everything needed for the document and open it afterwards in Skim.


<a id="compile-options"></a>
### Compile options


<a id="using-the-makefile-in-terminal"></a>
#####Using the makefile in Terminal
This makefile puts all the auxiliary files and folder in the *aux* folder to keep everything tidy.

* Run `make`. This will call `make clean`, `make single`, `make index`, `make glossaries`, `make bib`, `make double`, `make view`, `make counthtmlnoview`.      
* Run `make clean` to delete all non-essential files and folders.
* Run `make cleanfull` to delete all non-essential files and folders including the PDF document.
* Run `make view` to view the document in Skim.
* Run `make bib` to compile the bibliography.
* Run `make index` to compile the index.
* Run `make glossaries` to compile the glossaries.
* Run `make single` to compile the document once.
* Run `make double` to compile the document twice.
* Run `make cleandouble` to compile the document twice.


<a id="writing-the-commands-directly-in-terminal"></a>
##### Writing the commands directly in Terminal

When writing the commands directly in the terminal, the auxiliary files will not be placed in the *aux* folder. Instead they will be placed in directory in which *main.tex* is located.

* Run `pdflatex -shell-escape main` to compile the PDF a first time.
* Run `makeindex main` to compile the index.
* Run `makeglossaries main` to compile the glossaries, list of symbols and acronyms.
* Run `biber main` to compile the bibliography.
* Run `pdflatex -shell-escape main` (2x) to make the table of contents and ensure a correct hyperref linking.


<a id="other-options-from-the-makefile"></a>
##### Other options from the makefile

* Run `make count` to run texcount including the `\include` tex-files. Displays the result in terminal.
* Run `make counthtml` to run texcount and save the result in an html-file. The html-file will be opened afterwards.
* Run `make counthtmlnoview` to run texcount and save the result in an html-file. The html-file will NOT be opened afterwards.

If you do not change the index, the glossaries or the bibliography you only have to run `pdflatex -shell-escape main` once (`make single`) or twice (`make double`) to reflect the changes in the table of contents.


<a id="document-class-options"></a>
### Document class options
* `final`/`draft`: _draft_ shows:
  * an empty rectangle instead of the graphic
  * changes _SingleSpacing_ to _DoubleSpacing_
  * displays to-do notes as well as a list of these notes at the beginning of the document
* `oneside`/`twoside`: Underlying class is memoir
  * _twoside_ is for printing, new chapters start at odd page number.
  * The header for _oneside_ is identical for even and odd pages and has been compacted. This is recommended when the document is meant to be distributed digitally.
* `romanbackmatternumbering`: Continues roman numbering from front matter in the back matter when using _twoside_.
* `en`, `nl`, `fr`: Changes the language set in _babel_ and sets custom titles where needed.
* `gloss`: Enables glossaries
* `compacttitles`: Compacts all titles in the format _chapternumber_ _space_ _chaptertitle_ instead of '_Chapter_' _chapternumber_ _newline_ _chaptertitle_.
  * The chapter title becomes like the section title in the article class.
* `chapterraggedleft`: Aligns the chapter title to the right.
* `marginoffset`: Adds offset to margins for binding purposes when printing (only for _twoside_).
* `centerfigures`/`freefigures`: Turn automatic centering of floats on or off.
* `makeidx`: Enables the index.
* `highlightcode`: Enables the *Minted* package and a preset Arduino-style as well as a list of code if desired
* `tablecaptionontop`/`tablecaptiononbottom`: Self-explanatory: Puts the table caption on the top or the bottom of the table, default is on top.
* `csvsimple`: Adds the possiblity to add tables from *.csv* files. These can be given a style as well, manually or automatically.


<a id="document-definitions"></a>
### Document definitions
The margins can be changed from the default (top & bottom = 30mm [not including the header & footer], left & right = 25mm) as well as the binding offset (default 15mm).
To do so write the following lines in the preamble and change their values as you desire:

```
\def\leftsidemargin{25mm}
\def\rightsidemargin{25mm}
\def\topsidemargin{30mm}
\def\bottomsidemargin{30mm}
\def\bindingoffset{15mm}
```


<a id="defaults"></a>
### Defaults
* final, oneside, en, centerfigures (This is what you get if you do not fill in any options)


<a id="required-files"></a>
### Required Files
* `main.tex`
* `tvrmemoirclass.cls`
* `vubtitlepage.sty`
* `vubprivate.sty`


<a id="optional-files"></a>
### Optional Files

* `Makefile` if you want to compile the document in Terminal in a quick and easy way.
* `glossaries.tex` if you want glossaries.
* `acronyms.tex` if you want acronyms.
* `symbols.tex` if you want symbols.
* `zotero.bib` if you want a bibliography. I synced mine with Zotero and use automatic citing with LaTeXing. Change the bibliography name in its settings to reflect your bibliography name.
* All the files in the folder `input` if you want examples for most of the capabilities of this document class.


<a id="to-do-notes"></a>
### To-Do Notes
Provided are some to-do note styles, which can be very handy when proof-reading draft versions or when collaborating with others. A list of to-do notes is included as the first non-blank page following the title page. The different note styles are:

* `\unsure` : Light red note
* `\change` : Blue note
* `\info` : Green note
* `\improvement` : Lilac note
* `\thiswillnotshow` : This note will not be displayed in the PDF
* `\todo` : Orange note
* `\missingfigure` : Displays an upside down triangle inside a grey box the same width as the text

The last two style are default style provided with the `todonotes` package.


<a id="disclaimer"></a>
### Disclaimer
The documents found in this repository contains code that was found on StackExchange and similar sites as well as code that was written by me.
I do not claim to have written everything you will find in the files contained in this directory.


<a id="remarks"></a>
### Remarks
 * This class has been adapted from my _mybookclass_, which is why some things might not be optimized for use with the _memoir_ class.
 * The bibliography style is IEEE.
 * Where appropriate, the layout of the lists have been adapted to have a layout identical to the layout of the list of figures.
 * This class has been written on [macOS 10.13.5](https://www.apple.com/lae/macos/high-sierra/) using [Tex Live](https://www.tug.org/texlive/), [Sublime Text 3](https://www.sublimetext.com/3) and [LaTeXing](http://www.latexing.com/features.html) for Sublime text 2/3 and Terminal in macOS.
 * Minted requires [Pygments](http://pygments.org), which requires [Python](https://www.python.org).
 * To install these, [brew](http://brew.sh) and [Python](https://www.python.org) can be used.

 If you have *brew* installed you install Python by typing:

* `brew install python`

Now that python is installed you can use *pip* to install Pygments:

* `pip install pygments`


-
-
<a id="to-do"></a>
### To Do

* Add following as options instead of default:
  * Bibliography
  * Dual Bibliography
  * Add To-Do Notes Legend
  * Add explanation about over-/underfull boxes
  * Add DoubleSapcing as option
  * Add option for adapted page style for chapter with optional text in footer
  * Make Introduction, Summary, Abstract, Conclusion & Foreword automatic chapter names depending on language
  * See top of main.tex document.
  * Add -2.5mm horizontal space to VUB logo on title page.


<a id="known-issues"></a>
### Known Issues

* When using the option `fr` the pie chart option does not work.
* The link on the glossary-entries points to the first page of the document.
  * Fix: Load `hyperref` BEFORE `glossaries`.
  * Look at other packages which need to be loaded before or after `hyperref`.
* List of symbols page column title too long in Dutch, needs to be changed but seems to be language-specific.
* Fix page number link in index for acronyms (seems fixed when building with pdflatex twice before texindy)
* Make sans serif headings + headers optional
* Make two lines headers an option (only in digital style)
* Fix header layout for digital style with two-line headers
