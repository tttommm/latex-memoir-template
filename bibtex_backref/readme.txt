biblatex-ieee.bib is an adapted bib-file to be compatible with biblatex
IEEEexample.bib is the original bib-file which is only compatible with bibtex
This folder is meant to be used as test grounds for a backref-enabled bibtex

See https://tex.stackexchange.com/questions/15971/bibliography-with-page-numbers for instructions