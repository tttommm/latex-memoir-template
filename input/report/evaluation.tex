% !TEX root = ../../main.tex
% !TEX spellcheck=en_GB
\chapter{Evaluation of Platform and Algorithm} % (fold)
\label{cha:evaluation_of_platform_and_algorithm}
\section{Setup} % (fold)
\label{sec:setup}
The setup for the measurement is displayed in figure \ref{fig:rpibs} for the \rpibp. %
The setup is identical for the \rpib. %
The \rpizw setup is displayed in figure \ref{fig:rpizs}. %
The only difference with the setup of the \rpizw is the use of a \gls{usbotg} to connect the motherboard to the \rpi. %
For all runs of the program on the three \rpis, the same micro-SD card was used, without changing anything to its content, other then writing the timings. %

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{rpibs.png}
	\caption[Measuring setup for the \rpibp]{Measuring setup for the \rpibp. %
		On the upper left the multimeter for the temperature measurement. %
		On the bottom left the sensors. %
		On the bottom the Raspberry Pi 3B+. %
		In the middle a power meter connected to a dongle, which in term is connected to a laptop.
	}
	\label{fig:rpibs}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{rpizs.png}
	\caption[Measuring setup for the \rpizw]{Measuring setup for the \rpizw. %
		On the upper left the multimeter for the temperature measurement. %
		On the bottom left the sensors, connected via USB On The Go. %
		On the bottom the Raspberry Pi Zero W. %
		In the middle a power meter connected to a dongle, which in term is connected to a laptop.
	}
	\label{fig:rpizs}
\end{figure}

\par

This setup allows for the following measurements to be taken:

\begin{itemize}
	\item Ambient temperature
	\item \gls{soc} temperature
	\item Voltage
	\item Current
	\item Computation time
\end{itemize}

The computation time measurement is done in the software and saved in a \gls{csv} file. %
Several parts of the algorithm were timed separately, such as the time to get the sensor data, the interpolation time, the person detection time etc. %


% section setup (end)

\section{Results} % (fold)
\label{sec:results}
% \todo[inline]{Fill in results, with graphs if necessary}
% \info[inline]{Relevant graphs: trendline (multiple lines in one graphs), boxplot}
% \info[inline]{Relevant tables: average/median times, temperatures, power consumption, standard deviation?, computation time (briefly talk about separate times), display function time}

\subsection{Processing Power} % (fold)
\label{sub:processing_power}
The processing power required to run the software with and without animation was measured on the \rpizw, the \rpib, the \rpibp and a laptop for reference. %
The laptop is a \SI{15}{\inch} MacBook Pro Retina running macOS High Sierra with an Intel Core i7 7920HQ, an AMD Radeon Pro 560 graphics card with \SI{4}{\giga\byte} of GDDR5 memory and \SI{16}{\giga\byte} of \gls{ram}. %


\subsubsection{Without animation} % (fold)
\label{ssub:without_animation}
On the MacBook Pro the script uses $\approx\SI{38.9}{\mega\byte}$ of \gls{ram} and $\approx\SI{4.8}{\percent}$ of the \gls{cpu}. %
On the \rpib and the \rpibp the program uses $\approx\SI{15}{\mega\byte}$ of \gls{ram} and $\approx\SI{4}{\percent}$ of the \gls{cpu}. %
On the \rpizw the program uses $\approx\SI{16}{\mega\byte}$ of \gls{ram} and $\approx\SI{46}{\percent}$ of the \gls{cpu}. %

% subsubsection without_animation (end)
\subsubsection{With animation} % (fold)
\label{ssub:with_animation}
On the MacBook Pro the script uses $\approx\SI{91.2}{\mega\byte}$ of \gls{ram} and $\approx\SI{5.0}{\percent}$ of the \gls{cpu}. %
On the \rpib and the \rpibp the program uses $\approx\SI{45}{\mega\byte}$ of \gls{ram} and $\approx\SI{17}{\percent}$ of the \gls{cpu}. %
On the \rpizw however, although it has enough \gls{ram}, the \gls{cpu} is not fast enough to run the \python script at full speed.
% subsubsection with_animation (end)

\subsubsection{Comparison} % (fold)
\label{ssub:comparison_platform}
Comparing the numbers, it can be concluded that the program can be run with animation on the \rpib and \rpibp. %
The \rpizw however, is only fit to run the program without the animation. %
All the \rpis have more than enough \gls{ram} to run the programs. %
It should be noted that the figures are only approximations gathered from the Activity Monitor. %
The \rpibp is slightly faster than the \rpib, as was seen at the beginning of this section. %
% subsubsection comparison (end)
% subsection processing_power (end)

\subsection{Processing Time} % (fold)
\label{sub:processing_time}
An overview of the total time needed on average per iteration is given in table \ref{tbl:avg}. %
The most striking is the average iteration time of the \rpizw for the algorithm with the animation. %
Since each iteration uses five frames, and the sensor is set to 10 \gls{fps} (which is not 100\% accurate), to work in real-time the iteration time should be around half a second. %
The \rpibp has a beefier processor than the \rpib and this is reflected in the iteration time, there is a slight improvement. %
Whether or not the \rpi is in a case does not affect the iteration time. %

\input{tables/tbl_avg.tex}

The main parts of the algorithm were timed separately. %
An overview of the percentage of time needed per part is given in table \ref{tbl:allani} for the algorithm with the animation. %
An overview for the algorithm without the animation is given in table \ref{tbl:allnoani}. %
With the animation running, it is clear that the animation part is by far the slowest part of the algorithm, taking approximately 60\% for the \rpib and \rpibp and 83\% for the \rpizw. %
In second place is the time needed to read the data from the sensors, which of course is limited by the frame rate of said sensors. %
The pre-processing, which will be discussed in the next paragraph, takes almost no time at around 1\% compared to the rest of the algorithm. %
Finally, the person detection algorithm takes a little more time than the pre-processing but is still not a lot compared to the animation time. %
Looking at table \ref{tbl:allnoani}, displaying the percentages of time for the algorithm with no animation, the time needed to read out the sensors takes most of the time, varying from 89\% to 95\%. %
In this algorithm, the person detection takes up approximately 3.5\% for the \rpib and \rpibp, and more than twice that amount at almost 8\% for the \rpizw.

\input{tables/tbl_allani.tex}
\input{tables/tbl_allnoani.tex}
\par

The main parts of the pre-processing were also timed separately. %
An overview of the percentage of time needed for the frame setup time, reshaping time, blur time and interpolation time in relation to the total processing time is displayed in table \ref{tbl:preprocessing}. %
It is clear that the interpolation takes up the bulk of the time at around 64\%. %
Although the interpolation method used, \lanczos, is almost five times faster than using the spline method from \ref{ssub:spline}, it is the most computation intensive of the methods available in the \opencv function \verb|resize|. %
It was chosen because it gave noticeably better results than the other methods (linear, nearest neighbour, area and bicubic). %
The second highest time is the frame reshaping at approximately 10\% for the \rpib and the \rpibp and approximately 12\% for the \rpizw. %
Blurring is next at approximately 5\% of the pre-processing time. %
In last place is the frame setup time at less than 2\%. %

\input{tables/tbl_preprocessing.tex}

\subsubsection{Precessing time over the course of the measurements} % (fold)
\label{ssub:precessing_time_over_the_course_of_the_measurements}
To check for discrepancies over the course of the measurements, polynomials were calculated to display the iteration time. %
In figure \ref{fig:trend_rpi0w}, the iteration time of the \rpizw is displayed for a few different scenarios. %
These scenarios run the algorithm:
\begin{itemize}
	\item with the animation, but without a case,
	\item with the animation, and with a case,
	\item without the animation and without a case,
	\item and without the animation but with the case
\end{itemize}

It must be noted that the iteration time for the algorithm with the animation only takes into account the preparation for the animation. %
Still, The algorithms with the animation are a bit slower. %
When the \rpizw is in its case, it is slightly faster, but the differences over time are at least as high as the differences between the iteration time with and without the case. %

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{trend_rpi0w.png}
	\caption{Trendlines from the \rpizw iterations}
	\label{fig:trend_rpi0w}
\end{figure}

In figure \ref{fig:trend_rpi3b}, the iteration time of the \rpib is displayed, for the same scenarios as for the \rpizw. %
Again, there is a difference between the algorithm with and without the animation, the one with the animation being slightly slower. %
There is no difference worthy of mentioning between the iteration time when the \rpib is in its case or not. %

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{trend_rpi3b.png}
	\caption{Trendlines from the \rpib iterations}
	\label{fig:trend_rpi3b}
\end{figure}

In figure \ref{fig:trend_rpi3bp}, the iteration time of the \rpibp is displayed, again for the same scenarios. as for the \rpizw and the \rpib. %
The plot shows roughly the same as the graph from the \rpib, although the iteration times are slightly shorter. %

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{trend_rpi3bp.png}
	\caption{Trendlines from the \rpibp iterations}
	\label{fig:trend_rpi3bp}
\end{figure}


% subsubsection precessing_time_over_the_course_of_the_measurements (end)
% subsection processing_time (end)

\subsection{Power Usage and Temperatures} % (fold)
\label{sub:power_usage_and_temperatures}
The power usage of the board in different states (idle, without animation and with animation) is given in \ref{tbl:power}. %
Unsurprisingly, there is a rising trend in power usage from the top left to the bottom right, i.e., from a \rpizw at idle to a \rpibp running the algorithm with the animation. %
The increase in power usage between the algorithm without and with the animation is noticeably higher for the \rpib and the \rpibp than the increase for the \rpizw. %

\input{tables/tbl_power.tex}
\par

The temperature of the top of the \glspl{soc} was measured, as well as the ambient temperature at that time. %
The results of these measurements for the \rpizw, the \rpib and the \rpibp are displayed in tables \ref{tbl:rpizw_temp}, \ref{tbl:rpib_temp} and \ref{tbl:rpibp_temp} respectively. %
The \rpizw runs a little cooler than the \rpib, that in turns runs a little cooler than the \rpibp. %
When the \rpis are in their case, they run hotter than without the case. %
On average, they run approximately \SI{6}{\celsius} hotter. %
As seen in subsection \ref{sub:processing_time}, this increase in temperature does not affect the processing time. %

\input{tables/tbl_rpizw_temp.tex}
\input{tables/tbl_rpib_temp.tex}
\input{tables/tbl_rpibp_temp.tex}

The power usage of the board in different states was measured using a cheap \gls{usb} power meter. %
It is therefore meant as an indication and not as absolute values. %
% subsection power_usage_and_temperatures (end)

% \subsection{Speed vs Power Consumption} % (fold)
% \label{sub:price_vs_speed_vs_power_consumption}
% \todo[inline]{Fill-in make overview of the prices, the speed and power consumption and discuss.}
% % subsection price_vs_speed_vs_power_consumption (end)

% section results (end)
% chapter evaluation_of_platform_and_algorithm (end)
