% !TEX root = ../../main.tex
% !TEX spellcheck=en_GB
\chapter{Infrared Sensor} % (fold)
\label{cha:infrared_sensor}
As mentioned in \ref{ssub:grid_eye}, the \grideye has a viewing angle of \SI{60}{\degree}. %
Since we need to be able to monitor everything in front of the \grideye, multiple \grideyes are used. %
When using three \grideyes, a viewing angle of \SI{180}{\degree} was obtained. %

\par

Now the question of how to mount the \grideyes arises. %
Since the \grideye is packaged as a \gls{smd}, a \gls{pcb} needs to be designed to be able to interface with the sensor. %
Therefore, a breakout board was designed for one \grideye, displayed in figure \ref{fig:gebo}. %

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{gebo.png}
	\caption{Grid-EYE on breakout board}
	\label{fig:gebo}
\end{figure}

Three breakout boards were then placed in a 3D-printed bracket holding the sensors at \SI{60}{\degree} to each other, creating a \SI{180}{\degree} view. %
The drawback of using one \gls{pcb} per sensor is that for each sensor a cable with five connections on either side has to be made. %


% \begin{itemize}
% 	\item The size. %
% 		Each breakout board takes up quite a bit of space, in part because of the connector.
% 	\item Cabling. %
% 		Each breakout board has a connector with five lines, which has to be custom made and is quite laborious.
% 	\item Alignment. %
% 		In order to properly align the three sensors they should be carefully mounted on their breakout board, which in term should be carefully aligned on the bracket holding them at \SI{60}{\degree} to each other. %
% \end{itemize}

On the other hand, in case a sensor breaks it is easily replaced without the need to replace all three sensors, especially since they are glued to the \gls{pcb} for alignment. %
% In terms of alignment, when aligned with care, this turned out not to be an issue. %

\par

Since three sensors are connected, they must be multiplexed\index{multiplex}. %
As mentioned in \ref{ssub:grid_eye}, each \grideye can be given one of two \gls{i2c} addresses, by either pulling a pin low or pulling it high. %
Although the breakout board supports either address, by either leaving out a resistor or placing it, choosing the same address for each \grideye is faster in terms of production and programming. %
The multiplexer that was chosen is the Texas Instruments \href{http://bit.ly/2p5KlQt}{PCA9544A}. %
This is a low voltage, four-channel \gls{i2c} multiplexer with interrupt logic. %
The multiplexer is connected to an \gls{i2c} to \gls{hid} \gls{usb} protocol converter. %
In this case the Microchip \href{http://bit.ly/2LCsYCq}{MCP2221} was used. 
To connect the sensors, the multiplexer and the protocol converter, another \gls{pcb} was designed, which will be called the motherboard form now on. %
A picture form the motherboard, combined with the sensors on the bracket is displayed in figure \ref{fig:gemb}. %

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{gemb.png}
	\caption{Three Grid-EYE sensors connected to the motherboard}
	\label{fig:gemb}
\end{figure}

% chapter infrared_sensor (end)


\chapter{Image Processing Algorithm} % (fold)
\label{cha:image_processing_algorithm}
The image processing algorithm can be split up into the frame gathering (section\ref{sec:frame_gathering}), the frame preparation (section \ref{sec:frame_preparation}), background subtraction, and blob detection and counting. %

First, an overview of the algorithm, split into the frame gathering, the frame preparation and the person detection part is given. %
Secondly, the person detection algorithm will be explained in detail. %

\par

This chapter has a strictly confidential addendum, that has been added as appendix chapter \ref{cha:addendum} to this document. %


\section{Frame Gathering} % (fold)
\label{sec:frame_gathering}
To gather an entire frame, each sensor has to be addressed. %
Since the sensors are connected to a multiplexer, first the multiplexer will need to be set to the correct \gls{i2c} port. %
This is done by writing data to the control register. %
Then, to read the data form the sensor, the address of the first pixel's register is written to the \grideye. %
This will prompt the \grideye to send all the pixel data back. %
The data is sent per byte, with two bytes for each pixel. %
In our case, the high byte can be discarded because such high temperatures are not reached. %
This process has to be repeated for each sensor, three times in total for one entire frame. %

% section frame_gathering (end)
\section{Frame preparation} % (fold)
\label{sec:frame_preparation}
The algorithm to process the data from the \gls{ir} sensor array is the following. %
A certain number of frames is gathered, as explained in \ref{sec:frame_gathering}. %
The frames from each sensor are then averaged separately. %
They are then, again separately, reshaped into an 8 x 8 grid. %
Now the frames are stitched together, resulting in a 24 x 8 image. %
The frame is then blurred using a Gaussian blur\index{Gaussian blur}. %
Finally, the frame is interpolated using \lanczos. %


% \begin{enumerate}
% 	\item Get a certain number of frames from the three sensors and average them.
% 	\item Stitch the three averaged frames together. %
% 		The frame stitching assumes a proper alignment of the sensors.
% 	\item Blur the frame. %Gaussian Blur
% 	\item Interpolate the frame. %Lanzcos 4th order
% \end{enumerate}
% % section frame_preparation (end)


\section{Background: Initialisation, Subtraction and Update} % (fold)
\label{sec:background_initialisation_subtraction_and_update}
The process concerning the background can be summarized as follows, starting with the initialisation:

\begin{enumerate}
	\item Define a background frame from the average of 50 frames. %
		At this time, no persons should be in view of the sensors. %
	\item Quantize the background.
\end{enumerate}

Then, the background can be updated as follows:

\begin{enumerate}\setcounter{enumi}{2}
	\item Calculate the difference between a new, prepared frame and the background.\label{enum:diff}
	\item Calculate the threshold of the resulting difference frame.
	\item Retrieve the external contours using the calculated threshold. %
		The number of detected shapes of the highest hierarchy equals the number of detected persons.
	\item Update the background if persons were detected.
	\item Get a new averaged frame.
	\item Quantize the frame. %
	\item Go to step \ref{enum:diff}.
\end{enumerate}

% section background_initialisation_subtraction_and_update (end)

\section{Person Detection} % (fold)
\label{sec:person_detection}
% In this section the entire algorithm will be explained, the parts not pertaining to the person detection will be summarized.

Although the frame gathering and preparation and the background subtraction have been discussed already, they will be included again here. %
This time, the used functions will be mentioned.

\begin{enumerate}
	\item Calculate the background for the first time.
		%
		To get the background,
		\begin{enumerate}
			\item an average of 50 frames is taken. %
				To get a frame, it has to be transformed from a one-dimensional array to an 8 x 8 matrix. %
				This is done using \verb|numpy.reshape|.
			\item The resulting frame is blurred using Gaussian blur\index{Gaussian blur} with the eponymous \opencv function.
			\item This averaged frame is then interpolated using the \opencv \verb|resize| function. %
				The frame is interpolated to the fourth order using the \emph{\lanczos 4} method.
		\end{enumerate}
	\item Get an average of five frames, as explained earlier.\label{start_over}
		As for the background, the average of the frames is then blurred and interpolated.
	\item Now the person detection can start. %
		We start by preparing the frame by:
		\begin{enumerate}
			\item calculating the quantisation range, using \verb|numpy.arange|.
			\item Then the background is quantised using \verb|numpy.digitize|.
			\item The difference between the frame and the background is now calculated with an element-wise subtraction.
			\item The frame is then thresholded using the \opencv function \verb|threshold|, which returns a binary image.
			\item Using the \opencv function \verb|findContours|, the contours can now be found.
			\item The number of detected persons is equal to the number of detected contours from the highest level of the contour hierarchy.
		\end{enumerate}
	\item Now that the persons have been detected, the background can be updated using both the current background and the difference between the background and the current frame.
		\begin{enumerate}
			\item On the difference frame, a new binary threshold is applied.
			\item The result is inverted.
			\item The inverted result is used as a boolean array for indexing to adapt the background image on the pixels that are not persons using a predetermined weight.
		\end{enumerate}
	\item Go back to step \ref{start_over}.
\end{enumerate}

% section person_detection (end)
% chapter image_processing_algorithm (end)


%----------------------------------------------------------------------------
\chapter{Preparing the Raspberry Pi for Embedded Image Processing} % (fold)
\label{cha:preparing_the_raspberry_pi_for_embedded_image_processing}
Although a \rpi can be set up quite easily for general use, it is not quite a `plug and play' experience when it is meant for things like \gls{hid} communication, image processing, use of \opencv and displaying images. %
In this chapter, the process to get the \rpi ready for the person detection algorithm will be laid out. %

\par

First of all a micro-SD card was prepared with \rstretch. %
The kernel version 4.14.30-v7+ of \rstretch was used. %
Then all the dependencies and packages had to be installed, which is discussed in \ref{sec:installing_dependencies}. %

\par

Only the latest versions of the packages were used, which were the versions from March 2018. %
\opencv version 3.4.1 was installed, which was also the latest version at the time. %
Some packages are wrappers for libraries. %
The versions of which were also the latest at the time of installation. %

\section{Installing dependencies} % (fold)
\label{sec:installing_dependencies}
% To be able to run the existing \python code on a \rpi, all the dependencies had to be installed first. %
\subsection{HID} % (fold)
\label{sub:hid}
The first dependency is \hidapi. %
This is an \gls{api} which enables the user to interface with a device that communicates via \gls{hid}. %
It consists of both a \href{https://github.com/signal11/hidapi}{library} and a \python \href{https://github.com/trezor/cython-hidapi}{wrapper} for the library. %
Installation was done according to the tutorials on their respective GitHub pages. %
Both tutorials cover the installation of their prerequisites as well. %

% subsection hid (end)
\subsection{OpenCV} % (fold)
\label{sub:opencv}
To be able to use \opencv in \python, it had to be installed. %
For this a tutorial was used \cite{RosebrockRaspbianStretchInstall}, with some slight modifications. %
The first of which is that no virtual environment\index{virtual environment} was used. %
This was done because the \rpi will only be used for this script. %
% and because the script will be ported to \cpp for increased efficiency. %
% The porting is because \python is known to be inefficient\todo{add reference}, especially so on \gls{arm} architectures\todo{add reference}. %
As \opencv has dependencies of its own, those had to be installed as well. %
Most of them are covered in the tutorial mentioned above. %
One of those dependencies is \numpy, which is also used in the script. %
The dependencies that were not covered in the tutorial were found at step 5 of the tutorial, when \emph{cmake} is executed. %
An output is given which shows all the dependencies and if they were found or not. %
These dependencies can then be installed using:

\begin{lstlisting}
sudo apt-get install <package>
\end{lstlisting}

The second, main change, was the use of the latest \opencv version, which was version 3.4.1 at the time of installation, which was used instead of version 3.3.0 in the tutorial. %

% subsection opencv (end)
\subsection{Animation} % (fold)
\label{sub:animation}
To be able to display the window containing the images and the number of persons detected, the back-end \emph{\tkagg} has to be used. %
This has to be installed separately with this command: 

\begin{lstlisting}[language=bash]
sudo apt-get install tk-dev
\end{lstlisting}

Next, \matplotlib can be installed with the following command:

\begin{lstlisting}[language=bash]
sudo apt-get install python3-matplotlib
\end{lstlisting}

% subsection animation (end)

\subsection{Miscellaneous} % (fold)
\label{sub:miscellaneous}
Finally, \scipy has to be installed, which can be done by issuing the command 

\begin{lstlisting}[language=bash]
sudo apt-get install python3-scipy
\end{lstlisting}

Now all the dependencies are installed. %
After a reboot everything is ready to be tested. %
% subsection miscellaneous (end)
% section installing_dependencies (end)

\section{Implementing the Algorithm for Blob Detection for the Raspberry Pi} % (fold)
\label{sec:implementing_the_algorithm_for_blob_detection_for_the_raspberry_pi}
The original software was written in \matlab for three \grideye development boards. %
This not only had to be ported to \python, but also had to be adapted to use the newly designed hardware (see chapter \ref{cha:infrared_sensor}). %
Since \matlab has a lot of proprietary functions, it is not a matter of simply translating the code. %
A lot of functions that were used in \matlab are simply not available in another programming language. %
Therefore, alternatives had to be found.
\opencv offered quite a lot of those, for example for the thresholding and the blob detection. %
For the animation, \matplotlib was used. %
With its default back end, the program runs and a window pops up, but nothing is displayed. %
By default on \raspbian, \gtkagg is used. %
\tkagg had to be used to be able to view the \gls{gui}. %
By default \gls{blitting} is turned on in the animation function. %
Blitting\index{blitting} is a technique which dramatically improves the live performance of the animation and consists of combining several bitmaps together using a boolean function. %
This also prevents the output from being displayed. %
When turned off the program runs correctly. %

% \subsection{Relaying information to a website} % (fold)
% \label{sub:relaying_information_to_a_website}
% \todo[inline]{Explain http GET/SET to send number of persons to a website.}
% % subsection relaying_information_to_a_website (end)

% section implementing_the_algorithm_for_blob_detection_for_the_raspberry_pi (end)

\section{Running the software} % (fold)
\label{sec:running_the_software}
The script has to be run in \pythonthree because \hidapi requires it. %
On top of that, the script also has to be run in sudo mode because this permission is needed to access the USB ports. %

% section running_the_software (end)
% chapter preparing_the_raspberry_pi_for_embedded_image_processing (end)


%----------------------------------------------------------------------------
\input{input/report/evaluation.tex}

\chapter{Future Work} % (fold)
\label{cha:future_work}
To get the system to the next state, the following improvements are needed. %

\section{Software} % (fold)
\label{sec:software}
To improve the software, the reliability of the system could be tested in different situations. %
The influence of heat sources like the sun, or heaters could be characterised and implemented into the software. %

\subsection{Security} % (fold)
\label{sub:security}
In this thesis the subject of security on the communication level was not touched upon. %
This is however an important facet if the system were to be deployed commercially. %

% subsection security (end)
\subsection{Person detection algorithm} % (fold)
\label{sub:person_detection_algorithm}
Currently, the blob detection does not take into account the shape of the blob. %
By adding shape detection we could better differentiate between persons and objects. %

% subsection ir_sensor (end)

\section{Hardware} % (fold)
\label{sec:hardware}
\subsection{Stereoscopic view} % (fold)
\label{sub:stereoscopic_view}
If two sensors were to be arranged for a stereoscopic view, the three-dimensional shape of objects could be taken into account, thus further improving the accuracy of the person detection. %
% subsection stereoscopic_view (end)

\subsection{Power measurement} % (fold)
\label{sub:power_measurement}
The power consumption of the system was measured using a cheap \gls{usb} power meter. %
This kind of power meter is of course not very accurate and served only the purpose of giving an idea of what to expect. %
The power usage could be characterised using high-precision measuring instruments. %
% subsection power_measurement (end)

% To make the assembly easier, a flex-rigid \gls{pcb} is proposed.
% Although such a board increases the cost of the \gls{pcb}, it greatly reduces the assembly time.
% It could also reduce the size of the sensor array.

% section hardware (end)
% chapter future_work (end)


%----------------------------------------------------------------------------
\chapter{Conclusions} % (fold)
\label{cha:conclusions}
In the introduction, the problem was that persons needed to be detected whilst protecting their privacy. %
To this end, an ultra-low-resolution thermal camera, the \grideye, was used. %
The processing also had to be done on board, for which the \rpis have been selected. %

\par

Porting and adapting the software to use the new hardware proved to be a challenge, but was successfully realized. %
There are a lot of algorithms to choose from, each with their pros and cons. %
Weighing those pros and cons was necessary to get an algorithm that works, but is also computation efficient. %

\par

Preparing the \rpi for embedded processing was a meticulous, time-consuming job, but once it worked, it could be used for all three of the \rpis. %

\par

If an animation is desired, the \rpib or \rpibp should be used. %
If low power is more important than processing speed, the \rpib should be chosen. %
It uses considerably less power than the \rpibp but performs almost as well. %
For maximum performance the \rpibp should be used. %
If animation is not needed, the \rpizw is also suited. %
% The performance of the \rpis was not affected by a case.

% chapter conclusions (end)
