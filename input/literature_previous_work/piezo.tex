%!TEX root = /Users/Tom/BitBucket/latex-memoir-template/main.tex

\chapter[Samenvattende studie dichtheidsmeting]{Samenvattende studie omtrent een methode om de dichtheid te meten gebruik makende van piëzo-elektrische elementen}\label{c:piezo}
\section{Abstract}
In deze studie wordt de paper ``Ultrasonic Technique for Density Measurement of Liquids in Extreme Conditions''\cite{extreme} samengevat en besproken.
\bigbreak

\section{Meetopstelling}
In \fig{fig:opstelling} wordt de meetopstelling weergegeven. Men ziet dat de opstelling is opgebouwd uit een \pie dat vastgelijmd is aan een \emph{waveguide}. Op deze \emph{waveguide} is een \emph{matching layer} aangebracht die voor een grotere precisie zorgt. De vloeistof zit tussen twee van deze constructies. De informatie die men hieruit haalt zijn de verzonden golf \(U_{in}\), de terugekaatste golf \(U_r\) en de doorgestuurde golf \(U_{tr}\).
\par
Figuur \ref{fig:detail} toont de verschillende lagen tussen het \pie en de te meten vloeistof.

\begin{figure}
	\includegraphics[width=0.8\textwidth, trim={0cm 0cm 0cm 0cm},clip]{opstelling.png}%trim={<left> <lower> <right> <upper>}
	\caption[Ultrasone meting van de dichtheid: meetopstelling]{Ultrasone meting van de dichtheid: meetopstelling. Overgenomen uit ``Ultrasonic Technique for Density Measurement of Liquids in Extreme Conditions''\cite{extreme}.
		\label{fig:opstelling}}
\end{figure}

\begin{figure}
	\includegraphics[width=0.8\textwidth]{detail_waveguide.png}
	\caption[Model of the piezoelectric transducer with waveguide]
	{The mathematical model of the piezoelectric transducer with waveguide. Acoustic impedances of the elements: \(Z_d\), damping; \(Z_0\), piezo element; \(Z_1\), adhesive layer; \(Z_2\), waveguide; \(Z_3\), matching layer; \(Z_4\), measured medium; \(Z_{1G,2G}\), electrical impedances. Overgenomen uit ``Ultrasonic Technique for Density Measurement of Liquids in Extreme Conditions''\cite{extreme}.
		\label{fig:detail}}
\end{figure}

De \emph{transducer} en de \emph{waveguide} beschermen het \pie tegen hoge druk en temperatuur.
Stalen \emph{waveguides} met een aluminium poeder \emph{matching layer} geven de beste resultaten ten opzichte van temperatuursverschillen.

\section{Methode}
De meting van de dichtheid wordt uitgevoerd aan de hand van de meting van de ultrasone golf, gereflecteerd door de tip van \emph{waveguide 1}. Hiervoor worden \pies gebruikt. De \emph{trailing waves} kunnen overlappen met de golf die ons interesseert. Hierdoor is men genoodzaakt experimenteel te bepalen welk deel van het gemeten signaal in het tijdsdomein de nuttige informatie bevat. Dit wordt gedaan door een rechthoekig venster te gebruiken.

\par
Uit het verschil in amplitude van de verzonden en ontvangen golf kan men de reflectiecoëfficiënt berekenen. Deze wordt dan gebruikt om de akoestische impedantie van de vloeistof te bepalen. Tenslotte berekent men de dichtheid van de vloeistof aan de hand van de akoestische impedantie en de snelheid van de golf door de vloeistof.

\par
De akoestische impedantie is een maat voor de weerstand die een materiaal biedt voor een geluidsgolf. De eenheid is \([\si{\pascal\second\per\cubic\metre}]\).
\par
De verzonden puls door de \emph{interface matching layer}-vloeistof \(U_{tr}\) wordt gebruikt voor het meten van de snelheid \(c_3\) van de ultrasone golf in de te meten vloeistof. Dit gebeurt tijdens de calibratie-fase (zie \ref{sec:calibratie}).

\par
Van het ontvangen signaal \(U_r\) wordt uitsluitend de puls gereflecteerd door de tip van de \emph{waveguide} geselecteerd in het tijdsdomein en wordt vervolgens gebruikt om de dichtheid te bepalen van de vloeistof.

\par
Om preciezer te kunnen meten wordt de tip van \emph{waveguide 1} bedekt met een \emph{matching layer} met een dikte van \(\frac{1}{4}\)\supsc{de} golflengte.


\subsection{Het Signaal}
Voor de excitatie van de ultrasone golf werd een 3,3\si{\mega\hertz} sinusoïdaal signaal van drie perioden lang met een Gaussische envelop gebruikt. Deze envelop is nodig zodat het verstuurde en het gereflecteerde signaal van elkaar zouden kunnen worden gescheiden in het tijdsdomein. Men verkrijgt hierdoor een puls.

\subsection{Materiaalkeuze en -vereisten}
De vereisten voor de \emph{matching layer} zijn:
\begin{itemize}
	\item De akoestische impedantie van de \emph{matching layer} moet liggen tussen de impedantie van de \emph{waveguide transducer} en die van de vloeistof.
	\item De laag moet bestand zijn tegen hoge temperatuur en druk en stabiele akoestische eigenschappen vertonen over een bepaald temperatuurbereik.
\end{itemize}
Dit is een moeilijke opgave omdat er maar weinig materialen zijn die aan deze voorwaarden voldoen. Mogelijke materialen voor de \emph{matching layer} zijn:
\begin{itemize}
	\item Duralco 4703
	\item Polybenzimidazole (PBI)
	\item Aluminium poeder
	\item Glas poeder
\end{itemize}

De \emph{waveguides} moeten een redelijke thermische geleidbaarheid hebben. Er kan zowel staal als titanium gebruikt worden.

\subsection{Calibratie \label{sec:calibratie}}
Vooraleer men een meting kan uitvoeren moet de meetopstelling gecalibreerd worden. Men moet hiervoor eenmalig zo precies mogelijk de afstand tussen de tippen van de \emph{waveguides} bepalen. Als men de vertraging van de puls meet in transmissiemodus bij gekende temperatuur dan vind men deze afstand met de volgende formule:
\begin{equation}\label{lk}
	{l_k=c_w(T) \cdotp \tau_w(T)}
\end{equation}

%{\small
\begin{itemize}
	\item \(l_k\): Afstand tussen de tippen van de \emph{waveguides}.
	\item \(c_w(T)\): Gekende snelheid van de ultrasone golf in gedestilleerd water bij temperatuur \(T\).
	\item \(\tau_w(T)\): Vertraging in de tijd van de ultrasone puls in gedestilleerd water.
\end{itemize}%}

Er kan nu overgegaan worden naar de calibratie. Hiervoor moeten volgende stappen doorlopen worden:
\begin{enumerate}
	\item De gereflecteerde signalen \(U_r(T)\), \(U_{rw}(T)\) en de \emph{transmitted} signalen \(U_{tr}(T)\), \(U_{trw}(T)\) - verstuurd door gedestilleerd water en de te meten vloeistof - worden opgenomen.
	\item De tijdsvertraging in de te meten vloeistof wordt gemeten in transmissie-modus en de snelheid van de ultrasone longitudinale golf wordt bekomen uit:
	      \begin{equation}\label{c3}
		      {c_3(T)=\frac{l_k}{\tau_3(T)}}
	      \end{equation}
	      %{\small
	      \begin{itemize}
		      \item \(c_3(T)\): Snelheid van de ultrasone golf door het vloeibaar medium.
		      \item \(\tau_3(T)\): Vertraging in de tijd van de ultrasone puls in de vloeistof.
	      \end{itemize}%}
	\item De ratio's van de spectra van de gereflecteerde signalen \(U_r(T)\) en \(U_{rw}(T)\) - genormaliseerd ten opzichte van gedestilleerd water - worden berekend waarbij het signaal gereflecteerd door gedestilleerd water \(U_{rw}(T)\) als referentie dient.
	      \begin{equation}\label{x}
		      {x=\left[\frac{FFT(U_r(T))}{FFT(U_{rw}(T))}\right]_{|f=f_0}}
	      \end{equation}
	\item De akoestische impedantie \(Z_3\) wordt benaderd met volgende polynomiaal:
	      \begin{equation}\label{z3x}
		      {Z_3(x)=ax^2+bx+c}
	      \end{equation}
	      %{\small
	      \begin{itemize}
		      \item \(Z_3\): Akoestische impedantie van de vloeistof.
		      \item \(a\), \(b\) en \(c\) zijn de coëfficiënten afhankelijk van het materiaal op de \emph{waveguide} en de \emph{matching layer}.
	      \end{itemize}%}
	\item De calibratiefunctie wordt gevonden:
	      \begin{equation}\label{Urrw}
		      {\frac{U_r}{U_{rw}}=F(Z_3)}
	      \end{equation}
	      %{\small
	      \begin{itemize}
		      \item \(U_r\): Amplitude van het ultrasoon signaal gereflecteerd van de overgang van vast naar vloeibaar/
		      \item \(U_{rw}\): Amplitude van signaal gereflecteerd op gedestilleerd water.
		      \item \(F\): Calibratiefunctie.
	      \end{itemize}%}
	      waarbij de akoestische impedantie gegeven wordt door:
	      \begin{equation}\label{z3tx}
		      {{Z_3(T)=\rho_3(x) \cdotp c_3(T)}}
	      \end{equation}
	      %{\small
	      \begin{itemize}
		      \item \(\rho_3(x)\): Dichtheid van het vloeibaar medium.
	      \end{itemize}%}
\end{enumerate}

\section {Theoretische uitwerking} %voor het bekomen van de dichtheid aan de hand van piëzo-elektrische elementen}
Men bekomt de reflectie coëfficiënt \(R_3\) door volgende formule:

\begin{equation}\label{eq:r3}
	{R_3=\frac{U_{in}}{U_r}}
\end{equation}

%{\small
\begin{itemize}
	\item \(R_3\): Reflectie coëfficiënt van de ultrasonische golf van de overgang van vast naar vloeibaar.
	\item \(U_{in}\): Amplitude van het invallend ultrasoon signaal.
\end{itemize}%}

Om de nauwkeurigheid van de meting te verhogen wordt de tip van \emph{waveguide 1} bedekt met een matching layer van \(\frac{1}{4}\)\supsc{de} golflengte dik. Men kan nu de reflectie coëfficiënt vinden met volgende formule:
\begin{equation}\label{eq:r3ohm}
	{R_3(\omega)=\frac{Z_{in}(\omega)-Z_1}{Z_{in}(\omega)+Z_1}}
\end{equation}

%{\small
\begin{itemize}
	\item \(Z_{in}\): Input akoestische impedantie van de \emph{matching layer}.
	\item \(Z_1\): Akoestische impedantie van \emph{waveguide 1}.
\end{itemize}%}
\(R_3\) is nu afhankelijk van de frequentie. Als \(\omega=\omega_0=2\pi f_0\) waarvoor geldt dat

\begin{equation}\label{lambda}
	l=\frac{\lambda_0}{4} \text{\quad en \quad} \lambda_0=\frac{c_2}{f_0}
\end{equation}

%{\small
\begin{itemize}
	\item \(l\): De dikte van de \emph{matching layer}.
	\item \(\lambda_0\): De golflengte in de \emph{matching layer}.
	\item \(c_2\): De snelheid van de ultrasone golf in de \emph{matching layer}.
	\item \(f_0\): De resonantie frequentie.
\end{itemize}%}

dan vindt men voor \(Z_{in}(\omega_0)\)
\begin{equation}\label{eq:zin}
	{Z_{in}(\omega_0)=\frac{Z^2_2}{Z_3}}
\end{equation}

%{\small
\begin{itemize}
	\item \(Z_2\): Akoestische impedantie van het \emph{matching layer} materiaal.
\end{itemize}%}

Door \(Z_{in}\) uit vergelijking \ref{eq:zin} in te vullen in vergelijking \ref{eq:r3ohm} vindt men
\begin{equation}\label{z3}
	{Z_3=\frac{Z_2^2 \cdotp (1-R_3(\omega_0))}{Z_1 \cdotp (1+R_3(\omega_0))}}
\end{equation}

\(Z_1\) en \(Z_2\) moeten op voorhand gekend zijn. Voor een maximale precisie moet \(Z_2 \approx \sqrt{Z_1 \cdotp Z_3}\). Aangezien signalen in pulsvorm worden gebruikt, kan de amplitude uit het spectrum berekend worden:

\begin{equation}\label{ur}
	{U_r(f_0)=|FFT[U_l(t)]|_{|f=f_0}}
\end{equation}

%{\small
\begin{itemize}
	\item \(U_l(t)\): Signaal opgenomen door \emph{transducer 1}.
\end{itemize}%}

De amplitude van de invallende golf kan op deze manier niet berekend worden omdat hiervoor een aparte ontvanger van de invallende golf nodig is. De meting wordt vervangen door een meting van het gereflecteerd signaal van gedestilleerd water.
\begin{equation}\label{r3f0}
	{R_3(f_0)=\frac{U_r}{U_{rw}} \cdotp R_{3w}(f_0)}
\end{equation}

%{\small
\begin{itemize}
	\item \(R_{3w}\): Reflectie coëfficiënt van de ultrasonische golf voor water, bekomen tijdens de calibratie.
\end{itemize}%}

De akoestisch impedantie \(Z_3\) is afhankelijk van de temperatuur omdat zowel de dichtheid als de snelheid temperatuursafhankelijk zijn:
\begin{equation}\label{z3t}
	{Z_3(T)=\rho_3(T) \cdotp c_3(T)}
\end{equation}


Om de temperatuursafhankelijkheid van de dichtheid weg te werken wordt de snelheid van het signaal in de vloeistof gemeten in transmissiemodus.
\begin{equation}\label{rho3}
	{\hat{\rho}_3(T)=\frac{Z_3(T)}{\hat{c}_3(T)}}
\end{equation}


Het verband tussen de temperatuur en de golfsnelheid in vaste stoffen kan benaderd worden met volgende formule:
\begin{equation}\label{ct}
	{c(T)=c_0-T_K \cdotp \Delta T}
\end{equation}

%{\small
\begin{itemize}
	\item \(c(T)\): Snelheid van de ultrasone golf in een materiaal bij gegeven temperatuur \(T\).
	\item \(c_0\): Snelheid van de ultrasone golf bij kamertemperatuur (\(T_0=21^\circ C\)).
	\item \(T_K=\frac{c_0-c(T)}{\Delta T}\): Ultrasone snelheid-temperatuur coëfficiënt.
	\item \(\Delta(T)=T-T_0\): Temperatuur variatie in een materiaal.
\end{itemize}%}

\section{Voorwaarden en Beperkingen}

\subsection{Dikte van de lijmlaag}
Wanneer de dikte van de lijmlaag tussen het \pie en de \emph{transducer} toeneemt, neemt de uniformiteit van de frequentieresponsie af. Dit is niet handig aangezien men in puls-modus een vervorming van de puls begint waar te nemen. Bijgevolg moet de lijmlaag zo dun mogelijk zijn ($\leq$ 10\SI{10}{\micro\metre}).


\subsection{Akoestische impedantie van de lijmlaag}
Wanneer de akoestische impedantie van de lijmlaag daalt, daalt de uniformiteit van de frequentieresponsie. Bijgevolg moet de akoestische impedantie van de lijmlaag redelijk hoog zijn ($\geq$ 6-7 \si{\pascal\second\per\cubic\metre}).


\subsection{Lengte van de \emph{waveguides}}
Een lange \emph{waveguide} kan extra reflecties veroorzaken vanaf de zijkanten van de \emph{waveguide}. Een te korte \emph{waveguide} daarentegen kan de temperatuur niet voldoende doen dalen onder het aanvaardbaar niveau van 100\si{\celsius}. Bijgevolg is de ideale lengte van de \emph{waveguides} \(l=\SI{110}{\milli\metre}\).


\subsection{Onzekerheden}
De onzekerheden worden veroorzaakt door volgende zaken:
\begin{itemize}
	\item De kwantisatie van de signalen.
	\item De ruis van de voorversterker en andere elektr(on-)ische circuits.
	\item Variaties van de temperatuur in de \emph{matching layer} en de \emph{waveguides}.
\end{itemize}


\subsection{Precisie}
De experimenten die besproken worden in de paper die hier samengevat wordt hadden een standaarddeviatie van \SI{3.6e-3}{\gram\per\cubic\centi\metre}.

\par
De precisie waarmee de dichtheid gemeten wordt is afhankelijk van de variaties van de reflectiecoëfficiënt \(R_3\).

\section{Besluit}

De \emph{waveguides} dienen enkel om weerstand te bieden tegen een hoge druk en om de temperatuur te doen dalen tot een aanvaardbaar niveau dicht bij de sensor. In het geval van de etstank blijft men onder dit niveau. Hierdoor kan men stellen dat de \emph{waveguides} overbodig zijn aangezien men een hoge druk noch een hoge temperatuur heeft. Bijgevolg zijn veel formules niet meer bruikbaar. Dit is niet problematisch maar zorgt wel voor een verminderde precisie.